//
//  Artwork.swift
//  mapTest
//
//  Created by Michał Hęćka on 13/01/2019.
//  Copyright © 2019 Michał Hęćka. All rights reserved.
//

import UIKit
import MapKit

class Artwork: NSObject, MKAnnotation{
    //MARK: - initializing data structure
    let title : String?
    let locationName : NSMutableAttributedString
    var discipline : String
    @objc dynamic var coordinate : CLLocationCoordinate2D
    let symbol : String
    var imageName : String? = "point"
    var setNearest : Bool
    var typeOfAnnotation : String
    var userLocalisation : String? = "user"
    
    init(typeOfAnnototion : String, title: String, locationName: NSMutableAttributedString, discipline: String, nearest: Bool, coordinate: CLLocationCoordinate2D, symbol: String) {
        self.typeOfAnnotation = typeOfAnnototion
        self.title = title
        self.locationName = locationName
        self.discipline = discipline
        self.coordinate = coordinate
        self.symbol = symbol
        self.setNearest = nearest
        super.init()
    }
    
    var subtitle: NSMutableAttributedString? {
        return locationName
    }
    
    
    
    
    
    
    
    
}



class ArtworkView: MKAnnotationView {
    
    
    //MARK: - combining image nad title
    
    func combineImageAndTitle(image: UIImage, title: String) -> UIImage {
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 40, height: 18))
        label.numberOfLines = 1
        label.textAlignment = .center
        label.textColor = UIColor(named: "Color_grey")
        label.font = UIFont(name: "Helvetica-Bold", size: 11)
        label.text = title.replacingOccurrences(of: "<transport>", with: "")
        label.text?.removeFirst(7)
        
        
        
        
                                                                                //000                   FFFFFF
        let titleImage = ImageHelper.pgImage(textValue: label.text!, textBacColor: (UIColor.init(named: "Color_grey")?.toHexString()) ?? "000", textColor: (UIColor.init(named: "Color_grey_opposite")?.toHexString()) ?? "FFFFFF", fontSize: 12)//UIImage.imageFromLabel(label: label)
        let contextSize = CGSize(width: 40, height: 41)
        
        UIGraphicsBeginImageContextWithOptions(contextSize, false, UIScreen.main.scale)
        
        let rect1 = CGRect(x: 0, y: 0, width: Int(image.size.width * 0.5), height: Int(image.size.height * 0.5))
        image.draw(in: rect1)
        let rect2 = CGRect(x: (40 - Int(titleImage.size.width)) / 2, y: Int(image.size.height / 2 ), width: Int(titleImage.size.width), height: 15)
        titleImage.draw(in: rect2)
        
        let combineImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
                
        
        
        return combineImage!
    }
    
    
    override var annotation: MKAnnotation? {
        willSet {
            guard let artwork = newValue as? Artwork else {return}
            
            //clusteringIdentifier = UserAnnotationView.preferredClusteringIdentifier
            
            if artwork.typeOfAnnotation == "stop"{
                
                canShowCallout = true
                calloutOffset = CGPoint(x: 0, y: 0)
                
                let setButton = UIButton()
                setButton.frame.size.height = 30
                setButton.frame.size.width = 30
                setButton.setImage(UIImage(named: "fromgps"), for : .normal)
                rightCalloutAccessoryView = setButton
                
                let label = UILabel()
                label.numberOfLines = 0
                //label.font = label.font.withSize(10)
                //label.textColor = UIColor.init(named: "Color_subtitle")
                label.attributedText = artwork.subtitle// .replacingOccurrences(of: "<transport>", with: "")
                detailCalloutAccessoryView = label
                
            }
            else{
                canShowCallout = false
                rightCalloutAccessoryView = .none
                detailCalloutAccessoryView = .none
            }

            
            //MARK: - setting up right images to annorations
            
            let stop = artwork.discipline

            var size = CGSize(width: 39, height: 39)
            
            
            
            if artwork.setNearest == true && artwork.typeOfAnnotation == "stop"{


                if stop == "T" || stop == "TN" || stop == "NT" {
                    image = UIImage(named: "pointing-nearest-T")
                }
                else if stop == "A" || stop == "AN" || stop == "NA"{
                    image = UIImage(named: "pointing-nearest-A")
                }
                else if stop == "N" {
                    image = UIImage(named: "pointing-nearest-N")
                }
                else if stop == "TA" || stop == "AT" {
                    image = UIImage(named: "pointing-nearest-AT")
                }
                else{
                    image = UIImage(named: "pointing-nearest" )
                }
                size = CGSize(width: 50, height: 50)
            }
            else if artwork.typeOfAnnotation == "stop"{

                if stop == "T" || stop == "TN" || stop == "NT" {
                    image = UIImage(named: "pointing-T")
                }
                else if stop == "A" || stop == "AN" || stop == "NA"{
                    image = UIImage(named: "pointing-A")
                }
                else if stop == "N" {
                    image = UIImage(named: "pointing-N")
                }
                else if stop == "TA" || stop == "AT" {
                    image = UIImage(named: "pointing-AT")
                }
                else{
                    image = UIImage(named: artwork.imageName!)
                }
            }
            else if artwork.typeOfAnnotation == "tram"{
                image = combineImageAndTitle(image: UIImage(named: "tram_ico2")!, title: artwork.title!)
                size = CGSize(width: 50, height: 50)
            }
            else if artwork.typeOfAnnotation == "bus"{
                image = combineImageAndTitle(image: UIImage(named: "bus_ico3")!, title: artwork.title!)
                size = CGSize(width: 50, height: 50)
            }

            if artwork.typeOfAnnotation == "stop" {
                let pinImage = image
                centerOffset = CGPoint(x: 0, y: -(size.height / 2))
                UIGraphicsBeginImageContext(size)
                pinImage?.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
                image = UIGraphicsGetImageFromCurrentImageContext()
                UIGraphicsEndImageContext()
            }
        }
    }
}

//MARK: - making label  an image
extension UIImage {
    //label to an image
    
    class func imageFromLabel(label: UILabel) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(label.bounds.size, false, 0.0)
        label.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img!
    }
}

extension UIColor {
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0

        getRed(&r, green: &g, blue: &b, alpha: &a)

        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0

        return String(format:"#%06x", rgb)
    }
}
