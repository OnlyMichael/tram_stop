//
//  ArrivalViewController.swift
//  
//
//  Created by Michał Hęćka on 20/06/2019.
//

import UIKit
import SwiftyJSON
import Alamofire

class ArrivalViewController: UIViewController {

    
    @IBOutlet weak var dot1: UIImageView!
    @IBOutlet weak var dot2: UIImageView!
    @IBOutlet weak var dot3: UIImageView!
    @IBOutlet var outerView: UIView!
    
    @IBOutlet weak var cancelButton: DesignableButton!
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.view.window!.rootViewController?.dismiss(animated: false, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        deploy(param: parameterOfStop)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        animating(howMuch: 20)
    }
    //MARK: - animating bubbles
    func animating(howMuch : CGFloat) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
            UIView.animate(withDuration: 0.3, delay: 0, options: [.autoreverse, .repeat], animations: {
                self.dot1.frame.origin.y = howMuch
                
            })
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            UIView.animate(withDuration: 0.3, delay: 0, options: [.autoreverse, .repeat], animations: {
                self.dot2.frame.origin.y = howMuch
                
            })
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            UIView.animate(withDuration: 0.3, delay: 0, options: [.autoreverse, .repeat], animations: {
                self.dot3.frame.origin.y = howMuch
                
            })
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            UIView.animate(withDuration: 1, delay: 0, options: [], animations: {
                self.cancelButton.isHidden = false
            }, completion: nil)
        }
    }
    //MARK: - variables passed
    var parameterOfStop : String = ""
    var timeInterval : String = ""
    var openedSecondTime = false
    let lightImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: .light)
    
    var stopsJSON : JSON = ""
    var configJSON : JSON = ""
    var configBool : Bool = false
    
    var sv = UIView()
    
    var stop : String = ""
    var symbol : String = "symbol"
    var direction : String = ""
    
    var error : String = ""
    
    var routes = [String]()
    var params : [String : String] = [:]
    var paramsOrigo : [String : String] = [:]
    var listStops = [[(String)]]()
    var uiHistory = [[(String)]]()
    var listStops_tmp = [[(String)]]()
    

    let fileName = "StopsArray"
    var listArrivals = [[(Any)]]()
    var listRoute = [[[(Any)]]]()
    //MARK: - saving stop
    
    func showStops() {
        let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let fileURL = dir?.appendingPathComponent(fileName).appendingPathExtension("txt")
        
        let savedArray = NSArray(contentsOf: fileURL!) as? [[(String)]]
        
        //print("read from file : \(savedArray)")
        
        uiHistory = savedArray ?? []
        
    }
    
    func saveStops(StopsToSave : [[(String)]] ) {
        let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let fileURL = dir!.appendingPathComponent(fileName).appendingPathExtension("txt")
        
        uiHistory = StopsToSave
        
        (StopsToSave as NSArray).write(to: fileURL, atomically: true)
        
        //let savedArray = NSArray(contentsOf: fileURL!) as! [[(String)]]
        
        //print("saved to file : \(StopsToSave)")
        
    }
    //MARK: - deploying to origo
    func deploy(param : String) {
        
        
        
        timeInterval = String(NSDate().timeIntervalSince1970)
        
        let STOPS_URL = "https://api.origo.pl/?user_hash=\(UIDevice.current.identifierForVendor!.uuidString)&symbol="
        let STOPS_URL_STRAIGHT = "https://www.peka.poznan.pl/vm/method.vm?ts=\(self.timeInterval)"
        print("vendor \(UIDevice.current.identifierForVendor!.uuidString)")
        params = ["method" : "getTimes", "p0" : "{'symbol':'\(param.uppercased())'}"]
        paramsOrigo = ["symbol" : param.uppercased()]
        
        if param != "" {
            _ = getConfig(completion: {_ in
                if self.configBool == true {
                    _ = self.getTransportDataViaOrigo(url: STOPS_URL, parameters: self.paramsOrigo)
                    print("viaOrigo")
                }
                else if self.configBool == false {
                    _ = self.getTransportData(url: STOPS_URL_STRAIGHT, parameters: self.params)
                    print("viaPeka")
                }
                else {
                    self.error = "Brak połączenia z internetem"
                    self.insertError(stops_error: self.error)
                    //performSegue(withIdentifier: "ShowErrors", sender: self)
                    print("no internet connection in deploy")
                }
            })
            
        }
        else {
            error = "Nie zostały wprowadzone dane"
            insertError(stops_error: error)
            //performSegue(withIdentifier: "ShowErrors", sender: self)
        }
    }
    //MARK: configuration
    func getConfig(completion : @escaping(String) -> Void) {//-> JSON {
        let config = "https://api.origo.pl/?getConfig"
        let parmetersConfig : [String : String] = ["aa" : "getConfig"]
        
        Alamofire.request(config, method: .get, parameters: parmetersConfig, encoding: URLEncoding(destination: .queryString)).responseJSON {
            response in
            if response.result.isSuccess {
                print("Success in alamo getConfig!")
                self.configJSON = JSON(response.result.value!)
                self.configBool = self.configJSON["proxy"].boolValue


                //self.alert(alertCONFIG: JSON(response.result.value!))
            }
            else {
                print("Fail in alamoGetConfig")
                print( "\(String(describing: response.result.error))")
            }
            completion("")
            //print("state of configuration \(self.configJSON)")
        }
        
        func noInternet() {
            
            self.error = "Brak połączenia z internetem"
            self.insertError(stops_error: self.error)
            //self.performSegue(withIdentifier: "ShowErrors", sender: self)
            print("Fail in getConfig")
            
        }
        //return self.configJSON
    }
    //MARK: - fetching transport
    func getTransportDataViaOrigo (url: String, parameters: [String : String]) -> JSON{
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding(destination: .queryString)).responseJSON {
            response in
            if response.result.isSuccess {
                print("Success in origo alamo!")
                
                self.updateTransportData(json: JSON(response.result.value!))
            }
            else {
                
                
                print( "\(String(describing: response.result.error))")
                noInternet()
                print("Fail! in origo")
                
            }
            
        }
        func noInternet() {
            
            self.error = "Brak połączenia z internetem"
            self.insertError(stops_error: self.error)
            
            //self.performSegue(withIdentifier: "ShowErrors", sender: self)
        }
        
        return self.stopsJSON
    }
    
    func getTransportData(url: String, parameters: [String : String]) -> JSON{
        
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding(destination: .queryString)).responseJSON {
            response in
            if response.result.isSuccess {
                print("Success in alamo peka")
                self.updateTransportData(json: JSON(response.result.value!))
            }
            else {
                
                print("Fail! in peka")
                print( "\(String(describing: response.result.error))")
                noInternet()
                
            }
        }
        
        func noInternet() {
            self.error = "Brak połączenia z internetem"
            self.insertError(stops_error: self.error)
            //self.performSegue(withIdentifier: "ShowErrors", sender: self)
            
            
        }
        return self.stopsJSON
    }
    
    //MARK: - forming proper array to pass further
    func updateTransportData(json : JSON){
        //print(json)
        //print(json["success"]["bollard"]["name"])
        stop = json["success"]["bollard"]["name"].stringValue
        symbol = json["success"]["bollard"]["symbol"].stringValue
        direction = json["success"]["bollard"]["directions"].stringValue
        
        UIViewController.removeSpinner(spinner: sv)
        
        if json["success"]["bollard"]["name"] != JSON.null{
            
            if json["success"]["times"][0]["direction"] == JSON.null{
                print("json working in updateTransportData")
                saveStops(StopsToSave: uiHistory)
                uiHistory.removeAll()
                
                showStops()
            }
            else{
                
                stop = json["success"]["bollard"]["name"].stringValue
                symbol = json["success"]["bollard"]["symbol"].stringValue
                
                var arrivals = [Any]()
                
                var b = 0
                
                while b <= json["success"]["times"].count - 1{
                    
                    arrivals.append(json["success"]["times"][b]["direction"].stringValue)
                    arrivals.append(json["success"]["times"][b]["line"].stringValue)
                    arrivals.append(json["success"]["times"][b]["minutes"].stringValue)
                    arrivals.append(json["success"]["times"][b]["route_text_color"].stringValue)
                    arrivals.append(json["success"]["times"][b]["route_color"].stringValue)
                    arrivals.append(json["success"]["times"][b]["realTime"].boolValue)
                    arrivals.append(json["success"]["times"][b]["lowFloorBus"].stringValue)
                    arrivals.append(json["success"]["times"][b]["ticketMachine"].boolValue)
                    
                    //arrivals.append(json["success"]["times"][b]["route_desc"].stringValue)
                    listArrivals.append(arrivals)
                    arrivals.removeAll()
                    b += 1
                }
            }
            
            print(listArrivals)
            
            //saveStops(StopsToSave: uiHistory)
            
            performSegue(withIdentifier: "ShowStops", sender: self)
            
            showStops()
        }
        else {
            
            error = "Nie zostały wprowadzone poprawne dane"
            insertError(stops_error: error)
            performSegue(withIdentifier: "ShowErrors", sender: self)
            print("json not working bad input")
        }
        print("stop: \(stop) \nsymbol: \(symbol) \ndirection: \(direction) \nparam: \(params) \nconfigBool: \(configBool) \n")
        
    }
    func insertError(stops_error : String) {
        performSegue(withIdentifier: "ShowErrors", sender: self)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowStops" {
            let secondVC = segue.destination as! SecondViewController
            
            secondVC.stop = stop
            
            secondVC.listStops = listArrivals
            secondVC.params = params
            secondVC.paramsOrigo = paramsOrigo
            secondVC.configBool = configBool
            //secondVC.delegate = self as! CanReceive
            secondVC.symbol = symbol
            secondVC.direction = direction
            
            secondVC.openedSecondTime = openedSecondTime
            
        }
        else if segue.identifier == "ShowErrors" {
            let errorVC = segue.destination as! ThirdViewController
            
            errorVC.error = error
        }
    }
}
