//
//  GpsViewController.swift
//  Tram Stop
//
//  Created by Michał Hęćka on 22/02/2019.
//  Copyright © 2019 Michał Hęćka. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

import SwiftyJSON
import Alamofire

//class LocationManager: CLLocationManager{
//    var isUpdatingHeading = false
//    
//    static let shared = LocationManager()
//    
//    override func startUpdatingHeading() {
//        super.startUpdatingHeading()
//        isUpdatingHeading = true
//        
//    }
//    
//    override func stopUpdatingHeading() {
//        super.stopUpdatingHeading()
//        isUpdatingHeading = false
//    }
//}



class GpsViewController: UIViewController, CLLocationManagerDelegate, UIGestureRecognizerDelegate, MKMapViewDelegate ,textReceive {
    // MARK: - Variables map side

    var stopsArray = [[(Any)]]()
    weak var delegate : textReceive?
    
    
    var locationManager = CLLocationManager()
    var gpsStops : JSON = ""
    var userLatitude : Double = 0.0
    var userLongtitude : Double = 0.0
    var initialDownload = false
    var lastUserCoordinates = ["lat" : Double() , "lon" : Double()]
    var lastCenterCoordinates = ["lat" : Double(),"lon" : Double()]
    var stopNumber : Int = 0
    var stopsCount : Int = 0
    
    var stoppingFromUpdatingStops = false
    
    let screenSize: CGRect = UIScreen.main.bounds
    let lightImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: .light)
    
    //transport side
    var transportPos : JSON = []
    var transportArray = [[(Any)]]()
    var transportArray_after = [[(Any)]]()
    let userDefaults = UserDefaults.standard
    
    @IBOutlet weak var nearestButton: UIButton!
    @IBOutlet weak var heightNearestStop: NSLayoutConstraint!
    @IBOutlet weak var nearestStop: UIView!
    @IBOutlet weak var nearStop11: UILabel!
    @IBOutlet weak var stopType: UIImageView!
    @IBOutlet weak var gpsIndicator: UIActivityIndicatorView!
    @IBOutlet weak var alertLabel: UILabel!
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var opening: UIView!
    @IBOutlet weak var nearestStopView: UIView!
    
    //MARK: -
    
    @IBAction func showStop(_ sender: UIButton) {

        displayShownStop()
    }
    
    
    //MARK: - button back to user
    @IBAction func goBackToUser(_ sender: UIButton) {
        let initialLocation = CLLocation(latitude: userLatitude, longitude: userLongtitude)

        let center : CLLocationCoordinate2D = mapView.centerCoordinate
        
        let centerLatitude = center.latitude
        let centerLongtitude = center.longitude

        let latRest = (lastUserCoordinates["lat"]! - centerLatitude) * 1000
        let lonRest = (lastUserCoordinates["lon"]! - centerLongtitude) * 1000
        
        let stillPoint : Double = 4.0
 
        if (latRest >= -stillPoint && latRest <= stillPoint) && (lonRest >= -stillPoint && lonRest <= stillPoint){
            print("U are too close to initial location")
        }else{
            mapView.removeAnnotations(self.mapView.annotations)
            _ = getNearestStops(url: "https://api.origo.pl/?lat=\(userLatitude)&long=\(userLongtitude)", isInitial: true, userLong: userLongtitude, userLat: userLatitude)
            
            var region = self.mapView.region
            region.center = CLLocationCoordinate2DMake(userLatitude, userLongtitude)
            mapView.setCenter(region.center, animated: true)
        }
    }
    
    
    //MARK: - view did load and setup
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let panRight = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(panClosing(_:)))
        panRight.edges = .right
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swipeLeft(_:)))
        swipeLeft.direction = .left
        nearestStop.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight(_:)))
        swipeRight.direction = .right
        nearestStop.addGestureRecognizer(swipeRight)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapView(_:)))
        tap.numberOfTapsRequired = 1
        nearestStop.addGestureRecognizer(tap)
        
        let screenWidth = screenSize.width
        
        if screenWidth >= 375.0 {
            self.opening.addGestureRecognizer(panRight)
        }
        
        
        nearStop11.text = ""
        locationManager.delegate = self
        
        let annotation = MKPointAnnotation()
        mapView.register(ArtworkView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        mapView.delegate = self
        
        mapView.userLocation.title = "Twoja lokalizacja"
        mapView.showsScale = true
        
        //mapView.register(UserAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        //mapView.register(UserClusterAnnotationView.self, forAnnotationViewWithReuseIdentifier: MKMapViewDefaultClusterAnnotationViewReuseIdentifier)
        
        determineMyCurrentLocation()
        
        let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let fileURL = dir?.appendingPathComponent(fileName).appendingPathExtension("txt")
        //print(fileURL)
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        mapView.camera.heading = newHeading.magneticHeading
        print(newHeading.magneticHeading)
        print("heading")
        mapView.setCamera(mapView.camera, animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(GpsViewController.willEnterForeground),
            name: UIApplication.willEnterForegroundNotification, object: nil)
        
    }
    @objc func willEnterForeground() {
        initialDownload = false
        gpsIndicator.isHidden = false
        
        determineMyCurrentLocation()
    }
    
    
    @objc func panClosing(_ sender: UIGestureRecognizer) {
        performSegue(withIdentifier: "ShowView", sender: self)
    }
    
    //MARK: - swiping beetwen nearest stops
    var d = 0
    @objc func swipeLeft(_ sender: UIGestureRecognizer){
        
        UIView.animate(withDuration: 0.05, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: [], animations: {
            self.nearestStopView.frame.origin.x = -2
        }, completion: { (true) in
            UIView.animate(withDuration: 0.1, delay: 0, options: [], animations: {
                self.nearestStopView.frame.origin.x = 8
            })
            
        })
        
        var numberOfRepeats = 0
        
        while stopNumber + d <= 15 {

            if stopNumber + d == stopsCount{
                d = -1
                numberOfRepeats += 1
            }
            
            let nowStopNumber = stopNumber + d == -1 ? stopsCount : stopNumber + d
            d += 1
            print("swiped next, you were in \(nowStopNumber), now \(d)")
            
            if stopsArray[stopNumber + d][5] as! String != "" {
                
                showingNextStop(indexOfNextStop: stopNumber + d, indexOfNowStop: nowStopNumber, arrayOfNextStop: stopsArray[stopNumber + d], heading: mapView.camera.heading)
                break
            }
            
            if numberOfRepeats > 0 {break}
        }
    }
    
    @objc func swipeRight(_ sender: UIGestureRecognizer){
        
        UIView.animate(withDuration: 0.05, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: [], animations: {
            self.nearestStopView.frame.origin.x = 18
        }, completion: { (true) in
            
            UIView.animate(withDuration: 0.1, delay: 0, options: [], animations: {
                self.nearestStopView.frame.origin.x = 8
            })
        })
        
        var numberOfRepeats = 0
        
        while stopNumber + d >= 0 {
            
            if d == 0{
                d = stopsCount + 1 - stopNumber
                numberOfRepeats += 1
            }
            
            let nowStopNumber = d == 16 ? 0 : d
            
            d -= 1
            print("swiped previus, you were in \(nowStopNumber), now \(d)")
            if stopsArray[stopNumber + d][5] as! String != "" {
                
                showingNextStop(indexOfNextStop: stopNumber + d, indexOfNowStop: nowStopNumber, arrayOfNextStop: stopsArray[stopNumber + d], heading: mapView.camera.heading)
                
                break
            }
            
            if numberOfRepeats > 1 {break} 
        }
    }
    
    @objc func tapView(_ sender: UIGestureRecognizer){
        displayShownStop()
    }
    
    func displayShownStop(){
        lightImpactFeedbackGenerator.prepare()
        
        lightImpactFeedbackGenerator.impactOccurred()
        
        if stopsArray.indices.contains(stopNumber + d){
            parameterOfStop = stopsArray[stopNumber + d][4] as! String
            performSegue(withIdentifier: "Show", sender: self)
        }
    }
    
    func showingNextStop (indexOfNextStop : Int, indexOfNowStop : Int, arrayOfNextStop: Array<Any>, heading : Double){
        
        for annotation in self.mapView.annotations {
            if let title = annotation.title, !(title?.contains("<transport>"))! {
                self.mapView.removeAnnotation(annotation)
            }
        }
        setNameOfButtonsGps(set: indexOfNextStop, label: nearStop11)
        
        let nextStopLocation = CLLocation(latitude: arrayOfNextStop[2] as! CLLocationDegrees, longitude: arrayOfNextStop[3] as! CLLocationDegrees)
        
        var region = self.mapView.region
        region.center = CLLocationCoordinate2DMake(nextStopLocation.coordinate.latitude, nextStopLocation.coordinate.longitude)
    
        mapView.setCenter(region.center, animated: true)
        stoppingFromUpdatingStops = true
        
        
        let stop = arrayOfNextStop[5] as! String
        
        if stop == "T" || stop == "TN" || stop == "TN"{
            self.stopType.image? = UIImage(named: "pointing-nearest-T")!
        }
        else if stop == "A" || stop == "AN" || stop == "NA" {
            self.stopType.image? = UIImage(named: "pointing-nearest-A")!
        }
        else if stop == "N" {
            self.stopType.image? = UIImage(named: "pointing-nearest-N")!
        }
        else if stop == "TA" || stop == "AT" {
            self.stopType.image? = UIImage(named: "pointing-nearest-AT")!
        }
        else {
            self.stopType.image? = UIImage(named: "pointing-nearest")!
        }
        
        var F = 0
        
        var annotationsStops : Array = [MKAnnotation]()
        
        for annotation in mapView.annotations {
            if let title = annotation.title , !(title?.contains("<transport>"))! && (title?.contains("<\(stopsArray[indexOfNowStop][6])>"))!{//(title?.contains(stopsArray[indexOfNowStop][0] as! String))!{
                
                print("removed annotation \(annotation.title)")
                mapView.removeAnnotation(annotation)
            }
        }
        
        while F <= stopsArray.count - 1 {
            
            if stopsArray[indexOfNextStop][5] as! String != "" &&  stopsArray[F][4] as! String == arrayOfNextStop[4] as! String{
                print("nearest stop was added at position \(indexOfNextStop) and mode \(stopsArray[F][5] as! String)")
                
                mapView.addAnnotation(Artwork(typeOfAnnototion: "stop", title: stopsArray[F][0] as! String, locationName: setDirectionsAndRoundedArrivals(numberInJson: F), discipline: stopsArray[F][5] as! String, nearest: true, coordinate: CLLocationCoordinate2DMake(stopsArray[F][2] as! CLLocationDegrees, stopsArray[F][3] as! CLLocationDegrees), symbol: stopsArray[F][4] as! String))
            }
            else {
                mapView.addAnnotation(Artwork(typeOfAnnototion: "stop", title: stopsArray[F][0] as! String, locationName: setDirectionsAndRoundedArrivals(numberInJson: F), discipline: stopsArray[F][5] as! String, nearest: false, coordinate: CLLocationCoordinate2DMake(stopsArray[F][2] as! CLLocationDegrees, stopsArray[F][3] as! CLLocationDegrees), symbol: stopsArray[F][4] as! String))
            }
            
            F += 1
        }
    }
    //MARK: - map set up
    var regionRadius : CLLocationDistance = 350
    
    func centerMapOnLocation(location: CLLocation, isAnimated : Bool){
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        
        mapView.setRegion(coordinateRegion, animated: isAnimated)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location : CLLocation = locations.last!

        alertLabel.isHidden = true

        userLatitude = Double(round(10000 * location.coordinate.latitude)/10000)
        userLongtitude = Double(round(10000 * location.coordinate.longitude)/10000)

        if initialDownload == true  || (self.lastUserCoordinates["lat"] == userLatitude && self.lastUserCoordinates["lon"] == userLongtitude) {
            
            self.gpsIndicator.isHidden = true
        }
        else{
            self.mapView.removeAnnotations(self.mapView.annotations)
            _ = self.getNearestStops(url: "https://api.origo.pl/?lat=\(location.coordinate.latitude)&long=\(location.coordinate.longitude)", isInitial: true, userLong: location.coordinate.longitude, userLat: location.coordinate.latitude)
        }
        initialDownload = true

        self.lastUserCoordinates["lat"] = userLatitude
        self.lastUserCoordinates["lon"] = userLongtitude

    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location error - \(error)")
        locationManager.stopUpdatingLocation()
        locationManager.stopUpdatingHeading()
        locationManager.delegate = nil
        alertLabel.isHidden = false
        alertLabel.text = "Brak połączenia GPS"
    }

    func determineMyCurrentLocation () {
        
        gpsIndicator.startAnimating()
        
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.distanceFilter = 100
        locationManager.allowsBackgroundLocationUpdates = false
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        
        alertLabel.isHidden = true
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            locationManager.startMonitoringSignificantLocationChanges()
            
            alertLabel.isHidden = true
        }
        else {
            alertLabel.isHidden = false
            alertLabel.text = "Brak połączenia GPS"
        }
    }
    
    
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation, calloutAccessotyControlTaped control : UIControl!) -> MKAnnotationView? {
//
//        if annotation is MKUserLocation {return nil}
//        let reuseId = "pin"
//
//        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
//        if pinView == nil {
//            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
//            pinView!.canShowCallout = true
//            pinView!.animatesDrop = true
//            let calloutButton = UIButton(type: .infoLight)
//            pinView!.rightCalloutAccessoryView = calloutButton
//            pinView!.sizeToFit()
//        }
//        else {
//            pinView!.annotation = annotation
//        }
//
//        if annotation is Artwork {
//            print("its artwork")
//        }
//
//
//        return pinView
//    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if view.annotation is MKUserLocation {
            return
        }
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        
        let center : CLLocationCoordinate2D = mapView.centerCoordinate
        let centerLatitude = center.latitude
        let centerLongtitude = center.longitude
        
        let latRest = (lastCenterCoordinates["lat"]! - centerLatitude) * 1000
        let lonRest = (lastCenterCoordinates["lon"]! - centerLongtitude) * 1000
        
        var stillPoint : Double = 10.0
        
        alertLabel.isHidden = true
        
        if mapView.region.span.latitudeDelta >= 0.015{
            stillPoint = 8.0  //8
        }
        else {
            stillPoint = 6.0  //6 
        }
        
//        print(Double(round(centerLatitude * 1000) / 1000))
//        print(Double(round(lastUserCoordinates["lat"]! * 1000) / 1000))
//        if Double(round(centerLatitude * 1000) / 1000) != Double(round(lastUserCoordinates["lat"]! * 1000) / 1000)  && Double(round(centerLongtitude * 1000) / 1000) != Double(round(lastUserCoordinates["lon"]! * 1000) / 1000) {
//            locationManager.stopUpdatingHeading()
//            print("diffent place")
//        }
//        else{
//            if LocationManager.shared.isUpdatingHeading == false{
//                locationManager.startUpdatingHeading()
//            }
//            //locationManager.startUpdatingHeading()
//        }
        
        if (latRest >= -stillPoint && latRest <= stillPoint) && (lonRest >= -stillPoint && lonRest <= stillPoint) || stoppingFromUpdatingStops == true{
            print("too close to initial stop or its true")
            stoppingFromUpdatingStops = stoppingFromUpdatingStops == true ? false : stoppingFromUpdatingStops
            
        }
        else {
            print("its close to update stops")
            mapView.removeAnnotations(self.mapView.annotations)
            
            _ = getNearestStops(url: "https://api.origo.pl/?lat=\(center.latitude)&long=\(center.longitude)", isInitial: false, userLong: center.longitude, userLat: center.latitude)
            
            lastCenterCoordinates["lat"] = centerLatitude
            lastCenterCoordinates["lon"] = centerLongtitude
        }
    }
        
    var w = false
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if control == view.rightCalloutAccessoryView {
            
            var i = 0
            while i <= stopsArray.count - 1 {
                if view.annotation!.coordinate.latitude == stopsArray[i][2] as! Double || view.annotation!.coordinate.longitude == stopsArray[i][3] as! Double{
                    if w == false {
                        
                        self.dismiss(animated: false, completion: nil)
                        
                        parameterOfStop = "\(stopsArray[i][4])"
                        performSegue(withIdentifier: "Show", sender: self)
                        
                        //self.delegate?.dataRecived(symbol: self.gpsStops[i]["stop_code"].stringValue)
                        
                        w = true
                        _ = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { (Timer) in
                            self.w = false
                            print("curb closed")
                        }
                    }
                }
                i += 1
            }
        }
    }
    
    func getNearestStops(url: String , isInitial : Bool, userLong : Double, userLat : Double) -> JSON {
        
        gpsIndicator.isHidden = false
        
        Alamofire.request(url, method: .get, encoding: URLEncoding(destination: .queryString)).responseJSON {
            response in
            if response.result.isSuccess {
                
                self.gpsStops = JSON(response.result.value!)
                if self.gpsStops.count != 0 {
                    
                    self.gpsShow(gps: self.gpsStops, isInitial: isInitial, heading: self.mapView.camera.heading)
                    _ = self.getEveryTransport(long: userLong, lat: userLat)
                    self.refreshTransport(long: userLong, lat: userLat)
                    self.alertLabel.isHidden = true
                }
                else{
                    print("GetNearestStops json is empty")
                }
            }
            else if response.result.isFailure {
                
                print("Failure in getNearestStops")
                self.alertLabel.isHidden = false
                self.alertLabel.text = "Brak połączenia z internetem"
                
                self.gpsIndicator.isHidden = true

            }
        }
        return self.gpsStops
    }
    
    //MARK: - timer for transport update on map
    var timer_base : Timer?
    var timer : Timer?
    var howLong = 0
    
    func refreshTransport(long: Double, lat: Double){
        
        stopTimer()
        if timer_base == nil {
            timer_base = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.test), userInfo: ["lat": lat, "long": long], repeats: true)
        }
    }
    
    func stopTimer(){
        if timer_base != nil {
            timer_base!.invalidate()
            timer_base = nil
            
            transportArray.removeAll()
            howLong = 0
        }
    }
    
    @objc func test(timer: Timer) {
        
        if  let userInfo = timer.userInfo as? [String: Double],
            let lat = userInfo["lat"], let long = userInfo["long"] {
            
            howLong += 1
            
            print("Number of repeats : \(howLong)   \(lat) \(long)")
            _ = getEveryTransport(long: long, lat: lat)
        }
        else {
            stopTimer()
            print("timer invaildate")
        }
    }
    
    func getEveryTransport(long : Double, lat : Double) -> JSON{
        
        let url = "https://api.origo.pl/?getGTFSRT&long=\(long)&lat=\(lat)"
        
        Alamofire.request(url, method: .get, encoding: URLEncoding(destination: .queryString)).responseJSON {
            response in
            if response.result.isSuccess {
                
                self.transportPos = JSON(response.result.value!)
                
                if self.transportPos != 0 {
                    if self.userDefaults.bool(forKey: "transportVisible") != true {
                        self.gpsTransportShow(gps: self.transportPos)
                    }
                    else {
                        for annotation in self.mapView.annotations {
                            if let title = annotation.title , (title?.contains("<transport>"))! {
                                self.mapView.removeAnnotation(annotation)
                            }
                        }
                    }
                }
                else{
                    print("GetEveryTransport json is empty")
                }
                
                
                //self.gpsShow(gps: self.gpsStops, isInitial: isInitial)
                
            }
            else if response.result.isFailure {
                
                print("Failure in getEveryTransport")
                
            }
            
        }
        
        return self.transportPos
    }
    
    var animatedTooSoon = false
    func animateNearest(){
        
        
        if animatedTooSoon == false {

            DispatchQueue.main.asyncAfter(deadline: .now() + 0, execute: {
                
                UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: [.curveEaseOut], animations: {
                    self.nearestStopView.frame.origin.x = 0
                }, completion: { (true) in
                    
                    
                    UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0, options: [.curveEaseIn], animations: {
                        self.nearestStopView.frame.origin.x = 16
                    }, completion: { (true) in
                        
                        
                        UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseOut], animations: {
                            self.nearestStopView.frame.origin.x = 8
                        })
                        
                    })
                    
                })
            })
        }
        
        animatedTooSoon = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
            self.animatedTooSoon = false
        })
        
    }
    //MARK: - showing annotations on map
    func gpsShow (gps : JSON , isInitial : Bool , heading : Double = 0.0) {
        
        stopsArray.removeAll()
        mapView.removeAnnotations(mapView.annotations)
        
        var a = 0
        while a <= gpsStops.count - 1 {
            
            stopsArray.append([gpsStops[a]["stop_name"].stringValue])
            stopsArray[a].append(shortenDirections(suppliedString: gpsStops[a]["directions"].stringValue))
            stopsArray[a].append(gpsStops[a]["stop_latitude"].doubleValue)
            stopsArray[a].append(gpsStops[a]["stop_longitude"].doubleValue)
            stopsArray[a].append(gpsStops[a]["stop_code"].stringValue)
            stopsArray[a].append(gpsStops[a]["possible_mode"].stringValue)
            stopsArray[a].append(String(a))
            a += 1
        }
        stopsCount = stopsArray.count - 1
        
        
        //MARK-: here is location radius to change
        
        
        
        
        regionRadius = (Double(gpsStops[15]["distance"].stringValue) ?? 500.0)  * 2300
        
        
        d = 0 //resetting the counter to next stop
        
        if isInitial == true {
            let initialLocation = CLLocation(latitude: userLatitude, longitude: userLongtitude)
            centerMapOnLocation(location: initialLocation , isAnimated: false)
            
        }
        
        
        
        var i = 0
        var isAdded = false
        while i <= stopsArray.count - 1 {
            
            
            
            if stopsArray[i][5] as! String != "" && isAdded == false {
                print("nearest stop was added at position \(i) and mode \(stopsArray[i][5] as! String)")
                
                setNameOfButtonsGps(set: i, label: nearStop11)
                isAdded = true
                stopNumber = i
                
                mapView.addAnnotation(Artwork(typeOfAnnototion: "stop", title: stopsArray[i][0] as! String, locationName: setDirectionsAndRoundedArrivals(numberInJson: i), discipline: stopsArray[i][5] as! String, nearest: true, coordinate: CLLocationCoordinate2DMake(stopsArray[i][2] as! CLLocationDegrees, stopsArray[i][3] as! CLLocationDegrees), symbol: stopsArray[i][4] as! String))
            }
            else {
                mapView.addAnnotation(Artwork(typeOfAnnototion: "stop", title: stopsArray[i][0] as! String, locationName: setDirectionsAndRoundedArrivals(numberInJson: i), discipline: stopsArray[i][5] as! String, nearest: false, coordinate: CLLocationCoordinate2DMake(stopsArray[i][2] as! CLLocationDegrees, stopsArray[i][3] as! CLLocationDegrees), symbol: stopsArray[i][4] as! String))
            }
            
            i += 1
        }
        

        
        
        self.gpsIndicator.isHidden = true
        
        
        
        UIView.animate(withDuration: 0.3, animations: {
            self.heightNearestStop.constant = 150
            self.nearestStopView.isHidden = false
            
            self.view.layoutIfNeeded()
            
        }, completion: { finished in
            
            self.animateNearest()
            
        })
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {

            self.nearestButton.isHidden = false
            self.nearStop11.isHidden = false
            
            let stop = self.stopsArray[self.stopNumber][5] as? String
            
            if stop == "T" || stop == "TN" || stop == "TN"{
                self.stopType.image? = UIImage(named: "pointing-nearest-T")!
            }
            else if stop == "A" || stop == "AN" || stop == "NA" {
                self.stopType.image? = UIImage(named: "pointing-nearest-A")!
            }
            else if stop == "N" {
                self.stopType.image? = UIImage(named: "pointing-nearest-N")!
            }
            else if stop == "TA" || stop == "AT" {
                self.stopType.image? = UIImage(named: "pointing-nearest-AT")!
            }
            else {
                self.stopType.image? = UIImage(named: "pointing-nearest")!
            }
            
            self.view.layoutIfNeeded()
        })
    }
    
    func shortenDirections(suppliedString directionsFull : String, howMany : Int = 3) -> String{
        let directions = directionsFull.components(separatedBy: ",")
        
        var fullDirections : String
        
        //let times = howMany <= directions.count ? directions.count : howMany
        
        if directions.count > 2 {
            fullDirections = "\(directions[0]), \(directions[1]), \(directions[2])"
        }
        else if directions.count == 2 {
            fullDirections = "\(directions[0]), \(directions[1])"
        }
        else {
            fullDirections = "\(directions[0])"
        }
        
        return fullDirections
    }
    
    func gpsTransportShow(gps : JSON){
        
        transportArray_after = transportArray

        transportArray.removeAll()

        var a = 0
        while a <= transportPos.count - 1 {

            transportArray.append([transportPos[a]["vehicle"]["trip"]["route_id"].intValue])
            transportArray[a].append(transportPos[a]["vehicle"]["position"]["longitude"].doubleValue)
            transportArray[a].append(transportPos[a]["vehicle"]["position"]["latitude"].doubleValue)
            transportArray[a].append(transportPos[a]["id"].intValue)
            transportArray[a].append(transportPos[a]["vehicle"]["trip"]["trip_id"].stringValue)

            a += 1
        }
        var i = 0
//        if transportArray_after.isEmpty {
//            transportArray_after = transportArray
//            while i <= transportArray.count - 1 {
//
//                let type : String = transportArray[i][3] as! Int > 999 ? "bus" : "tram"
//
//                mapView.addAnnotation(Artwork(typeOfAnnototion: type, title: "\(transportArray[i][4] as! String)<transport>\(transportArray[i][0])", locationName: NSMutableAttributedString(string: "<transport>\(transportArray[i][4] as? String)"), discipline: "transport", nearest: false, coordinate: CLLocationCoordinate2DMake(transportArray[i][2] as! CLLocationDegrees, transportArray[i][1] as! CLLocationDegrees), symbol: ""))
//
//
//                i += 1
//            }
//            i = 0
//        }

        for ann in transportArray {
            var exists = false
            print(" \(transportArray)")
            for annotation in mapView.annotations {
                if let title = annotation.title , (title?.contains("<transport>"))! {

                    if (annotation.title??.contains(ann[4] as! String))!{
                        exists = true
                    }
                }
            }

            if !exists {
                let type : String = ann[3] as! Int > 999 ? "bus" : "tram"
                mapView.addAnnotation(Artwork(typeOfAnnototion: type, title: "\(ann[4] as! String)<transport>\(ann[0])", locationName: NSMutableAttributedString(string: "<transport>\(ann[4] as? String)"), discipline: "transport", nearest: false, coordinate: CLLocationCoordinate2DMake(ann[2] as! CLLocationDegrees, ann[1] as! CLLocationDegrees), symbol: ""))

            }
        }

        

        for annotation in mapView.annotations {
            if let title = annotation.title , (title?.contains("<transport>"))! {
                var isInNow = false
                var wasInThen = false

                var traA = 0
                for ann in transportArray {
                    if (annotation.title??.contains(ann[4] as! String))! {
                        isInNow = true
                        break
                    }
                    traA += 1
                }
                var traAa = 0
                for ann in transportArray_after {
                    if (annotation.title??.contains(ann[4] as! String))! {
                        wasInThen = true
                        break
                    }
                    traAa += 1
                }


                if  isInNow && wasInThen {

                    if transportArray[traA][2] as! Double != transportArray_after[traAa][2] as! Double && transportArray[traA][1] as! Double != transportArray_after[traAa][1] as! Double {
                        
                        
                        let type : String = transportArray[traA][3] as! Int > 999 ? "bus" : "tram"
                        
                        let annotation1 = Artwork(typeOfAnnototion: type, title: "\(transportArray[traA][4] as! String)<transport>\(transportArray[traA][0])", locationName: NSMutableAttributedString(string: "<transport>\(transportArray[i][4] as? String)"), discipline: "transport", nearest: false, coordinate: CLLocationCoordinate2DMake(transportArray_after[traAa][2] as! CLLocationDegrees, transportArray_after[traAa][1] as! CLLocationDegrees), symbol: "")
                        
                        let av = self.mapView.view(for: annotation)

                        
                        
                        mapView.addAnnotation(annotation1)
                        


                        _ = Timer.scheduledTimer(withTimeInterval: 0, repeats: false) { [weak self] _ in
                            self!.mapView.removeAnnotation(annotation)
                            UIView.animate(withDuration: 2.0 ,animations:  {
                                annotation1.coordinate = CLLocationCoordinate2D(latitude: self?.transportArray[traA][2] as! CLLocationDegrees, longitude: self?.transportArray[traA][1] as! CLLocationDegrees)
                            }, completion: nil)
                        }
                    }
                    print("found ann in new and old")
                    print(transportArray[traA])
                    print(transportArray_after[traAa])
                }
                else if !isInNow && !wasInThen {
                    print("old ann not found in new")
                    if (annotation.title??.contains("<transport>"))!{
                        self.mapView.removeAnnotation(annotation)
                        
                    }
                }
            }
        }
        
    /*
        for annotation in self.mapView.annotations {
            if let title = annotation.title , (title?.contains("<transport>"))! {
                self.mapView.removeAnnotation(annotation)
                
            }
        }
        
        transportArray.removeAll()
        var a = 0
        while a <= transportPos.count - 1 {
            
            transportArray.append([transportPos[a]["vehicle"]["trip"]["route_id"].intValue])
            transportArray[a].append(transportPos[a]["vehicle"]["position"]["longitude"].doubleValue)
            transportArray[a].append(transportPos[a]["vehicle"]["position"]["latitude"].doubleValue)
            transportArray[a].append(transportPos[a]["id"].intValue)
            transportArray[a].append(transportPos[a]["vehicle"]["trip"]["trip_id"].stringValue)
            
            a += 1
        }
        print(transportArray)
        var i = 0
        
        while i <= transportArray.count - 1 {
            let type : String = transportArray[i][3] as! Int > 999 ? "bus" : "tram"
            
            
            mapView.addAnnotation(Artwork(typeOfAnnototion: type, title: "<transport>\(transportArray[i][0])", locationName: NSMutableAttributedString(string: "transport"), discipline: "transport", nearest: false, coordinate: CLLocationCoordinate2DMake(transportArray[i][2] as! CLLocationDegrees, transportArray[i][1] as! CLLocationDegrees), symbol: ""))
            
            
            i += 1
        }
        */
    }
    
    //MARK: - setting name of nerarest stop buttons
    func setNameOfButtonsGps (set : Int, label : UILabel) {
        
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        let stopName = "\(gpsStops[set]["stop_name"].stringValue) "//(\(gpsStops[set]["stop_code"].stringValue))"
        var lines = "\(gpsStops[set]["possible_lines"].stringValue)"
        
        lines = lines == "" ? " " : lines
        
        let directions = "\n\(shortenDirections(suppliedString: gpsStops[set]["directions"].stringValue))"
        
        let attributedString = NSMutableAttributedString()
        
        let attributedStopName = NSMutableAttributedString(string: stopName)
        attributedStopName.setAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18),NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.init(named: "Color_grey")!], range: NSRange(location: 0, length: stopName.count))
        
        let attributedStopDirection = NSMutableAttributedString(string: directions)
        attributedStopDirection.setAttributes([NSAttributedString.Key.font: UIFont.italicSystemFont(ofSize: 13),NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.init(named: "Color_subtitle")!], range: NSRange(location: 0, length: directions.count))
        
        attributedString.append(attributedStopName)
        attributedString.append(setRoundedText(transportArray: formLinesArray(set: set, lines: lines), additional: [gpsStops[set]["stop_code"].stringValue]))
        attributedString.append(attributedStopDirection)
        
        label.attributedText = attributedString
    }
    
    func setRoundedText(transportArray : [[String : String]], transportColor : UIColor = .lightGray, additional : Array<String> = [], additionalColor : UIColor = .darkGray) -> NSMutableAttributedString{

        
        let fullString = NSMutableAttributedString()
    
        var transport = transportArray
        
        var number = 0
        if additional.count != 0{
            for item in additional {
                
                transport.insert(["num" : item,"background" : "", "foreground" : "<none>"], at: number)
                number += 1
            }
        }
        
        
        var i = 0
        while i < transport.count {
            
            if i == 5 {break}
            
            let alpha = transport[i]["background"] == "" ? 0 : 0.9
            
            let spaces : String = additional.count - 1 == i ? "  " : " "
            
            let normalNameString = NSMutableAttributedString.init(string: "")
            
            let attachment = NSTextAttachment()
            attachment.image = ImageHelper.pgImage(textValue: "\(transport[i]["num"] ?? "404")", textBacColor: "\(transport[i]["background"] ?? "525252")",alphaBack: CGFloat(alpha), textColor: "\(transport[i]["foreground"] ?? "FFFFFF")", borderSize: additional.indices.contains(i) ? 1.0 : 0)
            attachment.bounds = CGRect(x: 0, y: -6, width: (attachment.image?.size.width)!, height: (attachment.image?.size.height)!)
            normalNameString.append(NSAttributedString(attachment: attachment))
            fullString.append(normalNameString)
            
            fullString.append(NSMutableAttributedString(string: spaces))
            
            
            i += 1
        }
        return fullString
    }
    
    func formLinesArray(set : Int, lines : String) -> [[String : String]]{
        let linesArray = lines.components(separatedBy: ",")
        
        var linesAndColors = ["num" : String(), "background" : String(), "foreground" : String()]
        var finalArrayLines = [[String : String]]()
        
        var i = 0
        while i < linesArray.count{
            let linesColor = gpsStops[set]["possible_lines_color"][linesArray[i]]
            
            linesAndColors["num"] = linesArray[i]
            linesAndColors["background"] = linesColor[0].stringValue
            linesAndColors["foreground"] = linesColor[1].stringValue
            finalArrayLines.append(linesAndColors)
            
            linesAndColors.removeAll()
            i += 1
        }
        
        return finalArrayLines
    }
    
    func setDirectionsAndRoundedArrivals(numberInJson : Int) -> NSMutableAttributedString{
        
        let code = "\(gpsStops[numberInJson]["stop_code"].stringValue)"
        
        var lines = "\(gpsStops[numberInJson]["possible_lines"].stringValue)"
        lines = lines == "" ? "brak lini" : lines
        
        let linesArray = lines.components(separatedBy: ",")
        
        let attributedString = NSMutableAttributedString()
        
        let attributedStopDirection = NSMutableAttributedString(string: "\n\(stopsArray[numberInJson][1] as! String)")
        attributedStopDirection.setAttributes([NSAttributedString.Key.font: UIFont.italicSystemFont(ofSize: 12.5),NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.init(named: "Color_subtitle")!], range: NSRange(location: 0, length: (stopsArray[numberInJson][1] as! String).count + 1))
        if lines != ""{
            attributedString.append(setRoundedText(transportArray: formLinesArray(set: numberInJson, lines: lines), additional: [code]))
        }
        attributedString.append(attributedStopDirection)
        
        return attributedString
    }
    
    // MARK: - Variables arrivals side
    
    var timeInterval : String = ""
    
    
    var stopsJSON : JSON = ""
    var configJSON : JSON = ""
    var confingBool : Bool = true
    
    var routes = [String]()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    var params : [String : String] = [:]
    var paramsOrigo : [String : String] = [:]
    
    var sv = UIView()
    
    
    var stop : String = ""
    var symbol : String = "symbol"
    var direction : String = ""
    
    var error : String = ""

    var isChecked : Bool = false
    
    var listTimedStops = [[(String)]]()
    var listStops = [[(String)]]()
    var listStopsSpin = [String]()
    
    var parameterOfStop : String = ""
    
    var uiHistory = [[(String)]]()
    
    let fileName = "StopsArray"
    var listArrivals = [[(Any)]]()
    var listRoute = [[[(Any)]]]()
    
    //MARK: -
    func showStops() {
        let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let fileURL = dir?.appendingPathComponent(fileName).appendingPathExtension("txt")
        
        let savedArray = NSArray(contentsOf: fileURL!) as? [[(String)]]
        
        //print("read from file : \(savedArray)")
        listStopsSpin.removeAll()
        uiHistory = savedArray ?? []
        
    }
    
    func saveStops(StopsToSave : [[(String)]] ) {
        let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let fileURL = dir!.appendingPathComponent(fileName).appendingPathExtension("txt")
        
        uiHistory = StopsToSave
        
        (StopsToSave as NSArray).write(to: fileURL, atomically: true)
        
    }
    //MARK: - checking if user is old and making him old
    func userCheckIfNew (){
        
        let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let newUserFileURL = dir!.appendingPathComponent("fileName").appendingPathExtension("txt")
        
        let userData = NSArray(contentsOf: newUserFileURL)
        
        if userData == ["old"] {
            print("user is old")
        }
        else {
            print("user is new")
            userMakeOld()
        }
    }
    
    func userMakeOld(){
        
        let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let newUserFileURL = dir?.appendingPathComponent("fileName").appendingPathExtension("txt")
        
        let newUser : Array = ["old"]
        
        (newUser as NSArray).write(to: newUserFileURL!, atomically: true)
        
        performSegue(withIdentifier: "tutorial", sender: self)
    }
    //MARK: -
    
    func dataRecived(symbol: String) {
        print("from dataReceved : \(symbol)")
        parameterOfStop = symbol
        performSegue(withIdentifier: "Show", sender: self)
    }
    
    //MARK: - segueing
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        
        switch segue.identifier {
        case "tutorial":
            let tutorialVC = segue.destination as! TutorialViewController
            break
            
        case "ShowView":
            let gpsVC = segue.destination as! ViewController
            gpsVC.delegate = self as textReceive
            break
            
        case "Show":
            let arrivalVC = segue.destination as! ArrivalViewController
            arrivalVC.parameterOfStop = parameterOfStop
            break
            
        default:
            print("segue error")
        }
        
        

        
    }
}

//MARK: - image helper (making rounded background)

class ImageHelper{
    static func pgImage(textValue : String, textBacColor : String = "525252", alphaBack : CGFloat = 1, textColor : String, borderSize : CGFloat = 0, fontSize : CGFloat = 17) ->UIImage{
        
        let color : UIColor = textColor == "<none>" ? UIColor.init(named: "Color_grey")! : UIColor().HexToColor(hexString: "#\(textColor)")
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 15, height: 16))
        label.lineBreakMode = .byClipping
        label.textAlignment = .center
        label.textColor = textColor == "<none>" ? UIColor.init(named: "Color_grey")! : UIColor().HexToColor(hexString: "#\(textColor)")
        label.layer.borderColor = UIColor.init(named: "Color_grey")?.cgColor
        label.layer.backgroundColor = UIColor().HexToColor(hexString:"#\(textBacColor)").withAlphaComponent(alphaBack).cgColor
        label.layer.borderWidth = borderSize
        label.layer.cornerRadius = 5
        label.text = textValue == "" ? "@" : textValue
        label.font = UIFont.boldSystemFont(ofSize: fontSize)
        label.sizeToFit()
        label.bounds = CGRect(x: 0, y: 0, width: label.bounds.size.width + 4, height: label.bounds.size.height)

        UIGraphicsBeginImageContextWithOptions(label.bounds.size, false, UIScreen.main.scale)
        label.layer.allowsEdgeAntialiasing = true
        label.layer.render(in: UIGraphicsGetCurrentContext()!)

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
