//
//  ViewController.swift
//  Tram Stop
//
//  Created by Michał Hęćka on 22/02/2019.
//  Copyright © 2019 Michał Hęćka. All rights reserved.
//

import UIKit
import AVFoundation

import Alamofire
import SwiftyJSON

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)

    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    class func displaySpinner(onView : UIView) -> UIView {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.4)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center

        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }

        return spinnerView
    }
    class func removeSpinner(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
}

protocol textReceive : class {
    func dataRecived(symbol : String)
}

class ViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIGestureRecognizerDelegate, UIPickerViewDataSource{
    
    

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        return uiHistory.count
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        
        return 70
    }
    let userDefaults = UserDefaults.standard
    //MARK: - variable side
    var sv = UIView()

    var timeInterval : String = ""

    weak var delegate : textReceive?
    
    var gpsStops : JSON = ""
    var stopsJSON : JSON = ""
    var configJSON : JSON = ""
    var confingBool : Bool = true

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    var params : [String : String] = [:]
    var paramsOrigo : [String : String] = [:]

    var parameterOfStop : String = ""

    var stopNumber : String = ""

    var stop : String = ""
    var symbol : String = "symbol"
    var direction : String = ""

    var error : String = ""

    
    var listTimedStops = [[(String)]]()
    var listStops = [[(String)]]()
    var listStopsSpin = [String]()

    var helloWorldTimer : Timer? = nil

    var uiHistory = [[(String)]]()

    var listStops_tmp = [[(String)]]()
    //MARK: - outlet side

    @IBOutlet weak var scrollViewWithFavs: UIScrollView!
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var alertShow: UILabel!
    @IBOutlet weak var imageShow: UIImageView!
    @IBAction func saveTransportState(_ sender: Any) {
        userDefaults.set(userDefaults.bool(forKey: "transportVisible") == true ? false : true, forKey: "transportVisible")
    }
    

    @IBOutlet weak var transportState: UISwitch!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var spinView: DesignableView!
    @IBOutlet weak var spinHistory: UIPickerView!
    @IBOutlet weak var stopParam: UITextField!

    
    @IBAction func dismissing(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func ShowSecondVC(_ sender: UIButton) {

        let lightImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: .light)

        // Prepare shortly before playing
        lightImpactFeedbackGenerator.prepare()

        // Play the haptic signal
        lightImpactFeedbackGenerator.impactOccurred()

        if sender.tag == 2 {

            error = ""
            if stopParam.text != "" {
                //deploy(param: stopParam.text!)
                //print(stopParam.text!)
                self.dismiss(animated: false) {
                    self.delegate?.dataRecived(symbol: self.stopParam.text!)
                }
                //parameterOfStop = stopParam.text!
                //performSegue(withIdentifier: "ShowFromMenu", sender: self)
                
                
            }
            else {

                checkIfEmpty(UIElements: uiHistory)
            }
        }

        else if sender.tag == 4 {
            //deploy(param: uiHistory[spinHistory.selectedRow(inComponent: 0)][0])
            //print("lol \(uiHistory[spinHistory.selectedRow(inComponent: 0)][0])")
            self.dismiss(animated: true) {
                self.delegate?.dataRecived(symbol: self.uiHistory[self.spinHistory.selectedRow(inComponent: 0)][0])
                
            }
            //parameterOfStop = uiHistory[spinHistory.selectedRow(inComponent: 0)][0]
            //performSegue(withIdentifier: "ShowFromMenu", sender: self)
        }
    }
    
    //MARK: - view did load and gestuer recognizers
    override func viewDidLoad() {
        super.viewDidLoad()
        
        spinHistory.dataSource = self
        spinHistory.delegate = self
        
        stopParam.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        transportState.isOn = userDefaults.bool(forKey: "transportVisible") == true ? false : true
        
        scrollViewWithFavs.setContentOffset(CGPoint(x: 0, y: 80), animated: false)
        //        pressGesture.delegate = self
        //        tapGesture.delegate = self
        
        let swipeGestureClosing = UISwipeGestureRecognizer(target: self, action: #selector(swipeClosing(_:)))
        outerView.addGestureRecognizer(swipeGestureClosing)
        swipeGestureClosing.delegate = self
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(normalTap(_:)))
        tapGesture.numberOfTapsRequired = 1
        spinView.addGestureRecognizer(tapGesture)
        tapGesture.delegate = self
        
        let swipeGesture = UISwipeGestureRecognizer(target: self, action: nil)
        spinView.addGestureRecognizer(swipeGesture)
        swipeGesture.delegate = self
        
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        spinView.addGestureRecognizer(longGesture)
        longGesture.delegate = self
        
        self.hideKeyboardWhenTappedAround()
        
        let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let fileURL = dir?.appendingPathComponent(fileName).appendingPathExtension("txt")
        //print(fileURL)
        
        if NSArray(contentsOf: fileURL!) != nil {
            
            showStops()
        }
        spinHistory.selectRow(0, inComponent: 0, animated: false)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        
        otherGestureRecognizer.require(toFail: gestureRecognizer)
        return true
    }
    
    @objc func swipeClosing(_ sender: UIGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func normalTap(_ sender: UIGestureRecognizer){
        print("Normal tap")
        
        
        if uiHistory.count != 0  && over == false{
            
            print("This should be a param : \(uiHistory[spinHistory.selectedRow(inComponent: 0)][0])")
            self.dismiss(animated: true) {
//                self.parameterOfStop = self.uiHistory[self.spinHistory.selectedRow(inComponent: 0)][0]
//                self.performSegue(withIdentifier: "ShowFromMenu", sender: self)

                self.delegate?.dataRecived(symbol: self.uiHistory[self.spinHistory.selectedRow(inComponent: 0)][0])
                //self.parameterOfStop = self.uiHistory[self.spinHistory.selectedRow(inComponent: 0)][0]
            }
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification){
        
        if self.stopParam.frame.origin.y == 298{
            scrollViewWithFavs.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            self.stopParam.frame.origin.y = 1
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification){
        
        if self.stopParam.frame.origin.y != 298{
            self.stopParam.frame.origin.y = 298
            scrollViewWithFavs.setContentOffset(CGPoint(x: 0, y: 80), animated: true)
        }
    }
    
    let fileName = "StopsArray"
    var over = false
    @objc func longTap(_ sender: UIGestureRecognizer){
        print("Long tap")
        
        over = true
        
        spinHistory.isUserInteractionEnabled = false
        if uiHistory != [] {
            
            let alert = UIAlertController(title: "Czy chcesz usunąć ulubione przystanki?", message: "Tego kroku nie można będzie cofnąć.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Tak", style: .default, handler: { action in
                self.deleteStops()
                self.spinHistory.isUserInteractionEnabled = true
                self.over = false
            }))
            alert.addAction(UIAlertAction(title: "Nie", style: .cancel, handler: { action in
                self.spinHistory.isUserInteractionEnabled = true
                self.over = false
            }))
            
            self.present(alert, animated: true)
        }
        else {
            let logInErrorAlert = UIAlertController(title: "Uuups!", message: "Brak ulubionych przystanków", preferredStyle: .alert)
            logInErrorAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.deleteStops()
            self.present(logInErrorAlert, animated: true)
        }
        
        if sender.state == .ended {
            print("UIGestureRecognizerStateEnded")
        }
        else if sender.state == .began {
            print("UIGestureRecognizerStateBegan.")
        }
    }
    //MARK: - picker view set up
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label: UILabel
        
        if let view = view {
            label = view as! UILabel
        }
        else {
            label = UILabel(frame: CGRect(x: 0, y: 0, width: pickerView.frame.width, height: 400))
        }
        
        let stopsAndDirectionsArray = listStopsSpin[row]
        let fullString = "\(stopsAndDirectionsArray)"
        
        let attributedString = NSMutableAttributedString(string: fullString)
        
        let stopName = "\(uiHistory[row][1])"
        //let nameStopRange = (attributedString.string as NSString).range(of: stopName)
        
        var stopCode = " (\(uiHistory[row][0]))"
        //let stopCodeRange = (attributedString.string as NSString).range(of: stopCode)
        
        var directionsOfStop = ""
        
        if uiHistory[row].count < 3 {
            directionsOfStop = "\n\(uiHistory[row][0])"
            stopCode = ""
        }
        else {
            directionsOfStop = "\n\(uiHistory[row][2])"
        }
        //attributedString.setAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 10)], range: nameStopRange)
        //let directionsOfStop = fullString.replacingOccurrences(of: stopName, with: "")
        //print(directionsOfStop)
        //let title = NSAttributedString(string: stopsAndDirectionsArray, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20), NSAttributedString.Key.foregroundColor:UIColor.white])
        
        
        let attributedText = NSMutableAttributedString(string: stopName, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor :UIColor.init(named: "Color_grey")!])
        
        attributedText.append(NSAttributedString(string: stopCode, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.foregroundColor :UIColor.init(named: "Color_grey")!]))
        
        attributedText.append(NSAttributedString(string: directionsOfStop, attributes: [NSAttributedString.Key.font: UIFont.italicSystemFont(ofSize: 12), NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.init(named: "Color_subtitle")!]))
        
        
        label.attributedText = attributedText
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.sizeToFit()
        label.textAlignment = NSTextAlignment.center
        
        return label
    }
    
    //MARK: - deleting / saving / showing stops
    func deleteStops() {
        
        let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = dir!.appendingPathComponent(fileName).appendingPathExtension("txt")
        
        uiHistory.removeAll()
        
        //do {
            //try text.write(to: fileURL, atomically: false, encoding: String.Encoding.utf8)
            (uiHistory as NSArray).write(to: fileURL, atomically: true)
            
        //}
//        catch{
//            print(error)
//        }
        
        spinHistory.reloadAllComponents()
        checkIfEmpty(UIElements: uiHistory)
    }

    func saveStops(StopsToSave : [[(String)]] ) {
        let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let fileURL = dir!.appendingPathComponent(fileName).appendingPathExtension("txt")
        
        uiHistory = StopsToSave
        
        (StopsToSave as NSArray).write(to: fileURL, atomically: true)
        
        //let savedArray = NSArray(contentsOf: fileURL!) as! [[(String)]]
    }
    
    func showStops() {
        let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let fileURL = dir?.appendingPathComponent(fileName).appendingPathExtension("txt")
        
        let savedArray = NSArray(contentsOf: fileURL!) as! [[(String)]]
        
        listStopsSpin.removeAll()
        
        for a in savedArray{
            //listStopsSpin.append()
            
            var name : String = ""
            var code : String = ""
            name = String(a[1])
            code = String(a[0])
            
            var fullName = "\(name)\n\(code)" //dodane dwie linijki tekstu do scrollable view
            listStopsSpin.append(fullName)
            
        }
        
        uiHistory = savedArray
        
        func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
            let titleData = listStopsSpin[row]
            let myTitle = NSAttributedString(string: titleData, attributes: [NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.white])
            //print(listStopsSpin[row])
            
            return myTitle
        }
        spinHistory.reloadAllComponents()
        //checkIfEmpty(UIElements: savedArray)
    }
    
    func checkIfEmpty(UIElements : [[(String)]] ) {
        
        if UIElements.count == 0 {
            print("updated UI : \(UIElements)")
            
        }
        else if UIElements.count >= 1 {
            //print("\(uiHistory[spinHistory.selectedRow(inComponent: 0)][0])")
            //self.dismiss(animated: false, completion: nil)
            //parameterOfStop = uiHistory[spinHistory.selectedRow(inComponent: 0)][0]
            //performSegue(withIdentifier: "ShowFromMenu", sender: self)
            self.dismiss(animated: true) {
                
                self.delegate?.dataRecived(symbol: self.uiHistory[self.spinHistory.selectedRow(inComponent: 0)][0])
                
            }
        }
    }
    
    //MARK: - segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ShowFromMenu" {
            let secondVC = segue.destination as! ArrivalViewController
            
            secondVC.parameterOfStop = parameterOfStop
        }
    }
}
