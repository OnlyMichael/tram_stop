//
//  RouteViewController.swift
//  Tram Stop
//
//  Created by Michał Hęćka on 30/03/2019.
//  Copyright © 2019 Michał Hęćka. All rights reserved.
//

import UIKit


class RouteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if routes.count <= 20 {
//            return routes.count + 2
//        }
//        else {
//            return routes.count + 1
//        }
    
        return routes.count + 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.row == 0 {
            return 81
        }
        else{
            return 60
        }
    }
    
    //MARK: - forming route
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellDirection", for: indexPath) as! RoutesDirectionCellController
            
            cell.direction.text = direction
            cell.line.text = line
            cell.line.backgroundColor =  UIColor().HexToColor(hexString: backgroundLineColor)
            cell.line.textColor = UIColor().HexToColor(hexString: lineColor)
            
            return cell
        }
//        else if routeTableView.numberOfRows(inSection: 0) - 1 == indexPath.row && routes.count <= 20{
//            let cell = tableView.dequeueReusableCell(withIdentifier: "CellWithWarning", for: indexPath) as! WarningCellController
//
//            return cell
//        }
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellController", for: indexPath) as! RoutesTableViewCell
            //
            let charset = CharacterSet(charactersIn: symbol.lowercased())

            
            cell.stop.text = routes[indexPath.row - 1][0]
            cell.info.text = routes[indexPath.row - 1][2]
//            if routes[indexPath.row].count != 2 {
//                    cell.info.text = ""
//                }
//                else {
//
//                }
                if indexPath.row == 1 && routes[0][1].lowercased() == symbol.lowercased(){
                    cell.icon.image = UIImage.init(named: "routeIconDownClose")
                    cell.backGround.isHidden = false
                }
                else if indexPath.row == 1 {
                    cell.icon.image = UIImage.init(named: "routeIconDown")
                    cell.backGround.isHidden = true
                }
                else if routes[indexPath.row - 1][1] == routes[routes.endIndex - 1][1] && routes[indexPath.row - 1][1].lowercased() == symbol.lowercased() {
                    cell.icon.image = UIImage.init(named: "routeIconUpClose")
                    cell.backGround.isHidden = false
                }
                else if routes[indexPath.row - 1][1] == routes[routes.endIndex - 1][1]{
                    cell.icon.image = UIImage.init(named: "routeIconUp")
                    cell.backGround.isHidden = true
                }
                else if routes[indexPath.row - 1][1].lowercased() == symbol.lowercased() {
                    cell.icon.image = UIImage.init(named: "routeIconClose")
                    cell.backGround.isHidden = false
                    //routeTableView.scrollToRow(at: indexPath, at: .middle, animated: true)
                }
                else {
                    cell.icon.image = UIImage.init(named: "routeIcon")
                    cell.backGround.isHidden = true
                    
                }
            //print(numberOfStop)
            //print(route)
            //print("\(symbol.lowercased())  - stop")
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("stop opened as second : \(openedSecondTime)")
        
        if (indexPath.row != 0 && openedSecondTime != true) || openedSecondTime != true {
            symbolForSelectedStop = routes[indexPath.row - 1][1]
            performSegue(withIdentifier: "goBack", sender: self)
        }
    }
    //MARK: - variables
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//
//    }

    var routes : Array = [[(String)]]()
    
    var symbol : String = ""
    var line : String = ""
    var lineColor : String = ""
    var backgroundLineColor : String = ""
    var stop : String = ""
    var direction : String = ""
    var rowOfNearestStop : Int = 0
    
    var symbolForSelectedStop : String = ""
    var openedSecondTime = false
    var tooShortRoute = false
    //MARK: - view did load and forming route from raw file
    @IBOutlet weak var routeTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        routeTableView.delegate = self
        routeTableView.dataSource = self

        routeTableView.register(UINib(nibName: "RoutesTableViewCell", bundle: nil), forCellReuseIdentifier: "CellController")
        routeTableView.register(UINib(nibName: "RoutesDirectionTableViewCell", bundle: nil), forCellReuseIdentifier: "CellDirection")
        routeTableView.register(UINib(nibName: "WarningCell", bundle: nil), forCellReuseIdentifier: "CellWithWarning")
        //makeRoute(route: routesString)
        routeTableView.reloadData()
        
        //let lastRow = IndexPath.init(row: self.routeTableView.numberOfRows(inSection: 0) - 1, section: 0)
        DispatchQueue.main.async {
            if self.rowOfNearestStop + 1 < self.routeTableView.numberOfRows(inSection: 0) {
                let indexPath = IndexPath(row:  self.rowOfNearestStop , section: 0)
                self.routeTableView.scrollToRow(at: indexPath, at: .middle, animated: false)
            }
        }
    }
    
    func makeRoute (route : String) {
        //print(route)
        if route != "" {
            var i = 0
            let routeArray = route.components(separatedBy: " - ")
            //print(routeArray.count)
            
            while i <= routeArray.count - 1 {
                
                var b = [String]()
                if routeArray[i].contains("kurs"){
                    
                    if routeArray[i].contains("kurs skrócony do przystanku:") {
                        b.append(routeArray[i])
                        b[0] = b[0].replacingOccurrences(of: "kurs skrócony do przystanku: ", with: "", options: NSString.CompareOptions.literal, range: nil)
                        
                        b.append("kurs skrócony do przystanku:")
                    }
                    else if routeArray[i].contains("kurs wydłużony do przystanku:"){
                        b.append(routeArray[i])
                        b[0] = b[0].replacingOccurrences(of: "kurs wydłużony do przystanku: ", with: "", options: NSString.CompareOptions.literal, range: nil)

                        b.append("kurs wydłużony do przystanku:")
                    }
                    else if routeArray[i].contains("kurs skrócony do"){
                        b.append(routeArray[i])
                        b[0] = b[0].replacingOccurrences(of: "kurs skrócony do ", with: "", options: NSString.CompareOptions.literal, range: nil)
                        
                        b.append("kurs skrócony do")
                    }
                    else if routeArray[i].contains("kurs wydłużony do pętli: "){
                        b.append(routeArray[i])
                        b[0] = b[0].replacingOccurrences(of: "kurs wydłużony do pętli: ", with: "", options: NSString.CompareOptions.literal, range: nil)
                        
                        b.append("kurs wydłużony do pętli:")
                    }
                    else {
                        b.append(routeArray[i])
                        b[0] = b[0].replacingOccurrences(of: "kurs do  ", with: "", options: NSString.CompareOptions.literal, range: nil)
                        
                        b.append("kurs do")
                    }
                    
                    
                    if routeArray[i].contains("^"){
                        
                        b.append(routeArray[i])
                        b[0] = b[0].replacingOccurrences(of: "^R", with: "", options: NSString.CompareOptions.literal, range: nil)
                        b[0] = b[0].replacingOccurrences(of: "^M", with: "", options: NSString.CompareOptions.literal, range: nil)
                        b[0] = b[0].replacingOccurrences(of: "^S", with: "", options: NSString.CompareOptions.literal, range: nil)
                        b[0] = b[0].replacingOccurrences(of: "^N", with: "", options: NSString.CompareOptions.literal, range: nil)
                        b[0] = b[0].replacingOccurrences(of: "^A", with: "", options: NSString.CompareOptions.literal, range: nil)
                    }
                }
                else if routeArray[i].contains("^"){
                    
                    b.append(routeArray[i])
                    b[0] = b[0].replacingOccurrences(of: "^R", with: "", options: NSString.CompareOptions.literal, range: nil)
                    b[0] = b[0].replacingOccurrences(of: "^M", with: "", options: NSString.CompareOptions.literal, range: nil)
                    b[0] = b[0].replacingOccurrences(of: "^S", with: "", options: NSString.CompareOptions.literal, range: nil)
                    b[0] = b[0].replacingOccurrences(of: "^N", with: "", options: NSString.CompareOptions.literal, range: nil)
                    b[0] = b[0].replacingOccurrences(of: "^A", with: "", options: NSString.CompareOptions.literal, range: nil)
                }
                else {
                    b.append(routeArray[i])
                }
                routes.append(b)
                
                

                i += 1
            }
        }
    }
    //MARK: - opening a new secondView
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goBack" {
            let arrivalVC = segue.destination as! ArrivalViewController
            
            arrivalVC.parameterOfStop = symbolForSelectedStop
            arrivalVC.openedSecondTime = true
        }
    }
    
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
