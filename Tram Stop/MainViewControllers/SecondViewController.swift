//
//  SecondViewController.swift
//  Tram Stop
//
//  Created by Michał Hęćka on 15/09/2018.
//  Copyright © 2018 Michał Hęćka. All rights reserved.
//

import UIKit
import SpriteKit

import Alamofire
import SwiftyJSON



//MARK: - extension to form hex color -
extension UIColor{
    func HexToColor(hexString: String, alpha:CGFloat? = 1.0) -> UIColor {
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let alpha = alpha!
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = NSCharacterSet(charactersIn: "#") as CharacterSet
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
}



class SecondViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    //MARK: - table forming -
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //print(listStops[indexPath.row][0] as! String)
        

        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Name", for: indexPath) as! ArrivalName
            
            cell.name.text = stop
            cell.isUserInteractionEnabled = false
            return cell
        }
        if listStops.count != 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "arrivalCellController", for: indexPath) as! arrivalCellController
            
            let stopRow = indexPath.row - 1
            
            cell.line.backgroundColor = UIColor().HexToColor(hexString: "#\(listStops[stopRow].indices.contains(4) ? listStops[stopRow][4] : 575757)").withAlphaComponent(0.8)
            //print(stopRow)
            cell.lineDirection.text = listStops[stopRow][0] as? String
            cell.line.text = listStops[stopRow][1] as? String
            
            if "\(listStops[stopRow][4])" == "FFFFFF" {
                cell.line.textColor = UIColor.white
            }
            else if "\(listStops[stopRow][4])" == "" {
                cell.line.textColor = UIColor.white
                cell.line.backgroundColor = UIColor().HexToColor(hexString: "#525252").withAlphaComponent(0.8)
            }
            else {
                cell.line.textColor = UIColor().HexToColor(hexString: "#\(listStops[stopRow][3])")
            }
            
            
            if listStops[stopRow][2] as! String == "0"{
                
                cell.time.text = "zaraz"
                cell.time.backgroundColor = UIColor.red
                cell.time.textColor = UIColor.white
            }
            else if listStops[stopRow][2] as! String == ""{
                print("brak przyjazdów")
            }
            else{
                let (h, m, _) = secondsToHoursMinutesSeconds (seconds: Int(listStops[stopRow][2] as! String)! * 60)
                if h != 0 {
                    cell.time.text = "\(h) godz. \(m) min."
                }
                else {
                    cell.time.text = "\(m) min."
                }
                cell.time.backgroundColor = UIColor.clear
                cell.time.textColor = UIColor.init(named: "Color_grey")
            }
            
            
            
            let enter = (listStops[stopRow][6] as AnyObject)
            
            cell.gpsNotif.image = (listStops[stopRow][5] as AnyObject).boolValue == true ? UIImage.init(named: "gps1-on") : UIImage.init(named: "gps1-off") ?? UIImage.init(named: "gps1-off")
            
            let transportNumber = Int((listStops[stopRow][1] as? String)!) ?? 200
            
            
            
            if transportNumber < 30 && transportNumber != 201 {
                print("tram")
                if enter as! String == "true" {
                    cell.lowEntrance.image = UIImage.init(named: "low_entrance_on")
                }
                else if enter as! String == "false"{
                    cell.lowEntrance.image = UIImage.init(named: "high_entrance_on")
                }
                else {
                    cell.lowEntrance.image = UIImage.init(named: "low_entrance_off")
                }
            }
            else{
                if enter as! String == "true" || enter as! String == "false" {
                    cell.lowEntrance.image = UIImage.init(named: "low_entrance_on")
                }
                else {
                    cell.lowEntrance.image = UIImage.init(named: "low_entrance_off")
                }
            }
            //cell.lowEntrance.image = (listStops[stopRow][6] as AnyObject).stringValue == "true" ? UIImage.init(named: "low_entrance_on") : UIImage.init(named: "high_entrance_on") ?? UIImage.init(named: "low_entrance_off")
            
            cell.ticketMachine.image = (listStops[stopRow][7] as AnyObject).boolValue == true ? UIImage.init(named: "ticket_machine_on") : UIImage.init(named: "ticket_machine_off") ?? UIImage.init(named: "ticket_machine_off")
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "noArrivalCellController", for: indexPath) as! NoArrivalsCellController
            cell.isUserInteractionEnabled = false
            return cell
        }
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(listStops.count)
        if listStops.count == 0 {
            return 2
        }
        else{
            return listStops.count + 1
        }
        
        
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.row == 0 {
            return 54
        }
        else{
            return 105
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    let screenSize: CGRect = UIScreen.main.bounds
    
    let statusHeight : CGFloat = UIApplication.shared.statusBarFrame.size.height
    
    var openedSecondTime = false
    
    var timeInterval : String = ""
    var stopsJSON : JSON = ""
    var routesJSON : JSON = ""
    
    var helloWorldTimer : Timer?
    
    var STOPS_URL = ""
    var STOPS_URL_STRAIGHT = ""
    
    let fileName = "StopsArray"
    
    var params : [String : String] = [:]
    var paramsOrigo : [String : String] = [:]
    
    var configBool : Bool = false
    
    var uiHistory = [[(String)]]()
    
    var symbol : String = ""
    var stop : String = ""
    var direction : String = ""
    var index : Int = 0
    
    var listStops = [[(Any)]]()
    var listRoute = [[String]]()
    var routes = [[String]]()
    var directionRoute = ""
    
    var lineName = [UILabel]()
    var lineNumber = [UILabel]()
    var lineTime = [UILabel]()
    var lines = [Array<Any>]()
    
    var routeBackgroundlineColor = ""
    var routelineColor = ""
    var routeLine = ""
    var tooShortRoute = false
    
    @IBOutlet var StopsMainView: UIView!
    
    
    @IBOutlet weak var imageDisplayed: UIImageView!
    @IBOutlet weak var stops: UILabel!
    
    @IBOutlet weak var arrivalTableView: UITableView!

    var lineIndicator = [UIImageView]()
    var packs = [UIView]()
    
    @IBOutlet weak var ButtonView: UIView!
    @IBOutlet weak var favouriteButton: UIButton!
    
    @IBOutlet weak var alert1: UIView!
    
    var isChecked = false
    
    
    //MARK: - favourite button -
    @IBAction func favButton(_ sender: UIButton) {
    
        isChecked = !isChecked
        if isChecked {
            sender.setTitle("★", for: .normal)
            sender.setTitleColor(UIColor().HexToColor(hexString: "#ffed28"), for: .normal)
            
        } else {
            sender.setTitle("★", for: .normal)
            sender.setTitleColor(UIColor().HexToColor(hexString: "#e8e8e8"), for: .normal)
        }
        
    
        checkTheStop()
        print("now stop is \(isChecked)")
    }
    

    //MARK: - view did load and setup
    override func viewDidLoad() {

        super.viewDidLoad()
        
        arrivalTableView.delegate = self
        arrivalTableView.dataSource = self
        


        let stopImage = "appStops\(Int(arc4random_uniform(UInt32(11))))"
        imageDisplayed.image = UIImage(named: stopImage)

        let screenWidth = screenSize.width


        if screenWidth >= 375.0 {
            print("ran on iphone 6 and over")
        }
        else {
            print("ran on iphone se and under")
        }
        print(statusHeight)
        if listStops.isEmpty{

            update()
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                self.dismiss(animated: true, completion: nil)
            })
        }
        else {
            print("there is data")
            update()
            refreshData()
            
        }
        
        arrivalTableView.register(UINib(nibName: "arrivalCell", bundle: nil), forCellReuseIdentifier: "arrivalCellController")
        arrivalTableView.register(UINib(nibName: "NoArrivalsCell", bundle: nil), forCellReuseIdentifier: "noArrivalCellController")
        arrivalTableView.register(UINib(nibName: "ArrivalName", bundle: nil), forCellReuseIdentifier: "Name")
        
        let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let fileURL = dir?.appendingPathComponent(fileName).appendingPathExtension("txt")
        //print(fileURL)
        
        if NSArray(contentsOf: fileURL!) != nil {
            
            showStops()
        }
        
        
        arrivalTableView.reloadData()
        
    }
    
    
    

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopTimer()
        
        print("view disappeared")
        
    }
        
        
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        NotificationCenter.default.addObserver(
//            self,
//            selector: #selector(GpsViewController.willEnterForeground),
//            name: UIApplication.willEnterForegroundNotification, object: nil)
        print("view appeared")
        willEnterForeground()
        
    }
    @objc func willEnterForeground() {
        print("reopened")
        if listStops.isEmpty{

            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)//self.dismiss(animated: true, completion: nil)
            })
        }
        else {
            print("there is data")
            update()
            refreshData()
            
        }
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - saving stop if favourite button was tapped
    func alertGet(placeOfStop : Int){

        
        alert1.isHidden = false
    }
    
    func checkTheStop () {
        
        print(direction)
        print(symbol)
        print(stop)
        
        let directionsArray = direction.components(separatedBy: ",")
        print(directionsArray)
        var fullDirections : String
        if directionsArray.count >= 2 {
            fullDirections = "\(directionsArray[0]), \(directionsArray[1])"

        }
        else {
            fullDirections = "\(directionsArray[0])"

        }

        var i = uiHistory.count

        if isChecked == true {
            if i > 0 {
                
                var a = false
                
                
                for insideArray in uiHistory.reversed() {
                    
                    if insideArray[0] == symbol{
                     a = true
                    }
                }
                if a == false {
                    uiHistory.insert([symbol, stop, fullDirections], at: 0)
                }
                
            }
            else {
                uiHistory.insert([symbol, stop, fullDirections], at: 0)
            }
        }
        else {
            if i > 0 {
                for insideArray in uiHistory.reversed() {
                    
                    i -= 1
                    if insideArray[0] == symbol{
                        
                        print("true")
                        uiHistory.remove(at: i)
                        
                    }
                    else {
                        print("false")
                    }
                }
            }
        }
        saveStops(StopsToSave: uiHistory)
    }
    
    func saveStops(StopsToSave : [[(String)]] ) {
        let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let fileURL = dir!.appendingPathComponent(fileName).appendingPathExtension("txt")
        //print("in saveStops \(uiHistory)")
        
        
        (uiHistory as NSArray).write(to: fileURL, atomically: true)
        
        //let savedArray = NSArray(contentsOf: fileURL!) as! [[(String)]]
        
        //print("saved to file : \(StopsToSave)")
        
        
        
    }
    
    func showStops() {
        let dir = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let fileURL = dir?.appendingPathComponent(fileName).appendingPathExtension("txt")
        
        let savedArray = NSArray(contentsOf: fileURL!) as! [[(String)]]
        
        
        //print("read from file : \(savedArray)")
        
        uiHistory = savedArray
        
        let directionsArray = direction.components(separatedBy: ",")
        print(directionsArray)
        var fullDirections : String
        if directionsArray.count >= 2 {
            fullDirections = "\(directionsArray[0]), \(directionsArray[1])"

        }
        else {
            fullDirections = "\(directionsArray[0])"

        }
        
        var i = 0
        if uiHistory.count >= 1 {
            while i <= uiHistory.count - 1{
                if uiHistory[i][0] == symbol {
                    //print("its \(uiHistory[i][0]) ")
                    
                    uiHistory.remove(at: i)
                    uiHistory.insert([symbol, stop, fullDirections], at: 0)
                    isChecked = true
                    saveStops(StopsToSave: uiHistory)
                    break
                }
                else {
                    //print("its \(uiHistory[i][0]) \(symbol)")
                    i += 1
                    
                }
                
            }
        }
        if isChecked {
            favouriteButton.setTitle("★", for: .normal)
            favouriteButton.setTitleColor(UIColor().HexToColor(hexString: "#ffed28"), for: .normal)
            
        } else {
            favouriteButton.setTitle("★", for: .normal)
            favouriteButton.setTitleColor(UIColor().HexToColor(hexString: "#e8e8e8"), for: .normal)
        }
        
    }
    //MARK: - deploying when timer is running
    
    
    func deploy(param : String) {
        
        timeInterval = String(NSDate().timeIntervalSince1970)
        
        STOPS_URL = "https://api.origo.pl/?symbol="
        STOPS_URL_STRAIGHT = "https://www.peka.poznan.pl/vm/method.vm?ts=\(self.timeInterval)"
        
        //params = ["method" : "getTimes", "p0" : "{'symbol':'\(param)'}"]
        
        
            print(params)
            print("deployed")
        if configBool == true {
            _ = getTransportDataViaOrigo(url: STOPS_URL, parameters: paramsOrigo)
            print("Origo!")
        }
        else {
            _ = getTransportData(url: STOPS_URL_STRAIGHT, parameters: params)
            print("Peka!")
        }
        
        
        
    }
    
    var howLong = 0
    func refreshData (){
        
        if helloWorldTimer == nil {
            helloWorldTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.test), userInfo: nil, repeats: true)
        }

        
    }
    
    func stopTimer()
    {
        if helloWorldTimer != nil {
            helloWorldTimer!.invalidate()
            helloWorldTimer = nil
            howLong = 0
        }
    }
    
    @objc func test() {
        
        howLong += 1
        
//        let topIndex = IndexPath(row: 0, section: 0)
//        arrivalTableView.scrollToRow(at: topIndex, at: .top, animated: true)
        
        print("success \(howLong)")
        
        deploy(param: symbol)
        //getTransportData(url: STOPS_URL, parameters: params)
        
    }
    func getTransportDataViaOrigo (url: String, parameters: [String : String]) -> JSON{
        
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding(destination: .queryString)).responseJSON {
            response in
            if response.result.isSuccess {
                print("Success in alamo!")
                
                self.updateTransportData(json: JSON(response.result.value!))
                
                //print(JSON(response.result.value!))
                self.stopsJSON = JSON(response.result.value!)
            }
            else {

                print("Request not succeeded!")
                self.stopTimer()
                print( "\(String(describing: response.result.error))")

            }
        }

        
        return self.stopsJSON
    }
    
    func getTransportData(url: String, parameters: [String : String]) -> JSON{
        
        
        //var stop : String = "post-data='method=getTimes&p0={\(symbol):\(stopNumber)}'"
        
        print("alamofire")
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding(destination: .queryString)).responseJSON {
            response in
            if response.result.isSuccess {
                print("Success in alamo!")
                
                
                self.updateTransportData(json: JSON(response.result.value!))
                
                
            }
            else {
                
                self.stopTimer()
                print("Fail!")
                
                print( "\(String(describing: response.result.error ))")

            }
            
            
        }
        
        return self.stopsJSON
    }
    
    func updateTransportData(json : JSON){
        
        //print(json)
        if json["success"]["times"][0]["line"] != JSON.null{

            listStops.removeAll()

            
            stop = json["success"]["bollard"]["name"].stringValue

            var arrivals = [Any]()

            var b = 0
            print(json["success"]["times"].count)
            while b <= json["success"]["times"].count - 1{
                print("works",b)

                
                arrivals.append(json["success"]["times"][b]["direction"].stringValue)
                arrivals.append(json["success"]["times"][b]["line"].stringValue)
                arrivals.append(json["success"]["times"][b]["minutes"].stringValue)
                arrivals.append(json["success"]["times"][b]["route_text_color"].stringValue)
                arrivals.append(json["success"]["times"][b]["route_color"].stringValue)
                arrivals.append(json["success"]["times"][b]["realTime"].boolValue)
                arrivals.append(json["success"]["times"][b]["lowFloorBus"].stringValue)
                arrivals.append(json["success"]["times"][b]["ticketMachine"].boolValue)
                
                listStops.append(arrivals)
                arrivals.removeAll()
                b += 1
            }



            
            print("json working")

            self.update()
            
            //arrivalTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            
            self.arrivalTableView.reloadData()
            arrivalTableView.layoutIfNeeded()
            let indexPath = IndexPath(row: 0, section: 0)
            self.arrivalTableView.scrollToRow(at: indexPath, at: .top, animated: true)
        }
        else {

            
            print("json not working")
        }
        
    }
    //MARK: - getting and forming route
    func getRoute (url: String, parameters: [String : String], index : Int) -> JSON{
    
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding(destination: .queryString)).responseJSON {
        response in
            if response.result.isSuccess {
                print("Success in alamo!")
        
                
                self.makeRoute(json: JSON(response.result.value!), index: index)
                //print(JSON(response.result.value!))
                
            }
            else {
        
                
                self.stopTimer()
                print( "\(String(describing: response.result.error))")
        
            }
        }
    
        return self.routesJSON
    }
    //MARK: - showing route is on -
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("row: \(indexPath.row)")
        var line = listStops[indexPath.row - 1][1] as! String
        routeLine = line
        
        routelineColor = listStops[indexPath.row - 1][3] as! String
        routeBackgroundlineColor = listStops[indexPath.row - 1][4] as! String
        
        var direction = listStops[indexPath.row - 1][0] as! String
        
        directionRoute = direction
        line = line.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        direction = direction.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!

        _ = getRoute(url: "https://api.origo.pl/?line=\(line)&direction=\(direction)", parameters: params, index : indexPath.row)


    }
    var rowOfNearestStop : Int = 0
    
    func makeRoute(json : JSON, index : Int){
        //print(json)
        var route = [String]()
        var a = 0
        
        var numberOfStopInRouteJson = 0
        var positionOfStopInRouteJson = 0
        
        var existsInRoute = false
        var directionInRouteExists = false
        
        var startOfRoute = 0
        
        while a <= json["route_bollards"].count - 1 {
            let symbolRoute : String = json["route_bollards"][a]["tag"].stringValue
            
            if symbolRoute == symbol {
                numberOfStopInRouteJson = json["route_bollards"][a]["orderNo"].intValue
                positionOfStopInRouteJson = a
                
                startOfRoute = positionOfStopInRouteJson - numberOfStopInRouteJson + 1
                
                print("Stop in route is on position : \(numberOfStopInRouteJson)")
                print("Stop in json is on position : \(positionOfStopInRouteJson)")
                print("First stop in this route is on position : \(startOfRoute)")
                print(json["route_bollards"][startOfRoute])
                var b = 0

                while b <= json["route_bollards"].count - 1 {
                    
                    let rightStop = b + startOfRoute
                    var name = ""
                    let stopNo : Int = json["route_bollards"][rightStop]["orderNo"].intValue
                    
                    if b <= stopNo{
                        name = json["route_bollards"][rightStop]["name"].stringValue
                        name.removeLast(5)
                        route.append(name)
                        route.append(json["route_bollards"][rightStop]["tag"].stringValue)
                        route.append(json["route_bollards"][rightStop]["orderNo"].stringValue)
                        listRoute.append(route)
                        route.removeAll()
        
                        if symbol == json["route_bollards"][rightStop]["tag"].stringValue {
                            rowOfNearestStop = b
                            print("scroll to \(rowOfNearestStop)")
                            existsInRoute = true
                        }
        
                        if json["route_bollards"][rightStop]["name"].stringValue.contains(directionRoute) {
                            print("the direction exists as \(directionRoute)")
                            directionInRouteExists = true
                        }
                        
                    }else {
                        print("this stop is lower than previous, next number would be \(json["route_bollards"][startOfRoute]), initiating BREAK")
        
                        break
                    }
                    b += 1
                }
                
                
                
                
            }
            a += 1
        }

        print("list \(listRoute)")
        
        if listRoute.count <= 10 {
            
            tooShortRoute = true
        }
        
        if listRoute.isEmpty == false && existsInRoute == true && directionInRouteExists == true {
            //print(listRoute)
            
            routes = listRoute 
            performSegue(withIdentifier: "route", sender: self)
        } else {
            
            let alert = UIAlertController(title: "Brak dostępnego rozkładu jazdy dla lini \(routeLine) w tym kierunku.", message: "Sprwadż ponownie później.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { action in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }
        listRoute.removeAll()
    }
    //MARK: - segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "route" {
            
            let VC = segue.destination as! RouteViewController
            VC.symbol = symbol
            VC.line = routeLine
            VC.lineColor = routelineColor
            VC.backgroundLineColor = routeBackgroundlineColor
            VC.stop = stop
            VC.routes = routes
            VC.direction = directionRoute
            VC.rowOfNearestStop = rowOfNearestStop
            VC.openedSecondTime = openedSecondTime
            VC.tooShortRoute = tooShortRoute
        }
    }
    
    
    func update() {
        print("update")

        //stops.text = stop
    }
    
    

    @IBAction func goBack(_ sender: Any) {
        
        let lightImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: .light)
        
        // Prepare shortly before playing
        lightImpactFeedbackGenerator.prepare()
        
        // Play the haptic signal
        lightImpactFeedbackGenerator.impactOccurred()
        
        stopTimer()
        
        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)//self.dismiss(animated: true, completion: nil)
        
        if #available(iOS 10.3, *){
            ReviewMe().showReviewView(afterMinimumLaunchCount : 10)
        }else{
            //Review View is not available on this version of iOS
        }

    }
    

    

}
