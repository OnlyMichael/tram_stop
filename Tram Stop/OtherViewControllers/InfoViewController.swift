//
//  InfoViewController.swift
//  Tram Stop
//
//  Created by Michał Hęćka on 08/12/2018.
//  Copyright © 2018 Michał Hęćka. All rights reserved.
//

import UIKit

class InfoViewController: UIViewController {

    let versionText = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String
    let buildText = Bundle.main.infoDictionary?["CFBundleVersion"] as? String
    @IBOutlet weak var version: UILabel!
    @IBOutlet weak var appDescription: UILabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        version.text = "Version: \(versionText!) (\(buildText!))"
        version.textColor = UIColor.init(named: "Color_grey")
        
        appDescription.textColor = UIColor.init(named: "Color_grey")
        // Do any additional setup after loading the view.
    }
    

    @IBAction func CloseInfo(_ sender: Any) {
    
        let lightImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: .light)
        
        // Prepare shortly before playing
        lightImpactFeedbackGenerator.prepare()
        
        // Play the haptic signal
        lightImpactFeedbackGenerator.impactOccurred()
        
        self.dismiss(animated: true, completion: nil)
    
    }
    
    

}
