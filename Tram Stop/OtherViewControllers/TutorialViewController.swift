//
//  TutorialViewController.swift
//  Tram Stop
//
//  Created by Michał Hęćka on 22/12/2018.
//  Copyright © 2018 Michał Hęćka. All rights reserved.
//

import UIKit
import SpriteKit

class TutorialViewController: UIViewController {

    let screenSize: CGRect = UIScreen.main.bounds
    var screenHeight : CGFloat = 0.0
    @IBOutlet weak var centerImage: NSLayoutConstraint!
    @IBOutlet weak var phoneDisplayed: UIImageView!
    
    @IBOutlet weak var buttonClose: UIButton!
    
    @IBOutlet weak var legend: NSLayoutConstraint!
    
    @IBOutlet weak var menu: NSLayoutConstraint!
    @IBOutlet weak var blueStop: NSLayoutConstraint!
    @IBOutlet weak var orangeStops: NSLayoutConstraint!
    @IBOutlet weak var legendStops: NSLayoutConstraint!
    @IBOutlet weak var nearestStop: NSLayoutConstraint!
    
    
    @IBOutlet weak var orangeStop_text: UILabel!
    
    @IBOutlet weak var blueStop_text: UILabel!
    
    @IBOutlet weak var menu_text: UILabel!
    
    @IBOutlet weak var nearest_text: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenHeight = screenSize.height
        
        menu_text.textColor = UIColor.init(named: "Color_grey")
        orangeStop_text.textColor = UIColor.init(named: "Color_grey")
        blueStop_text.textColor = UIColor.init(named: "Color_grey")
        nearest_text.textColor = UIColor.init(named: "Color_grey")
        
        
        legend.constant = -180
        blueStop.constant = 220
        orangeStops.constant = 220
        legendStops.constant = 220
        nearestStop.constant = 220
        
        print(screenHeight)
        
        if screenHeight >= 750{
            
            phoneDisplayed.image = UIImage(named: "tutorial2_transparent")
        }
        else {
            
            phoneDisplayed.image = UIImage(named: "tutorial1_transparent")
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
            
            UIView.animate(withDuration: 0.7){
                
                self.centerImage.constant = -150
                
                self.legend.constant = 10
                
                self.view.layoutIfNeeded()
            }
        })
        // Do any additional setup after loading the view.
    }
    var i = 1
    func showingTutorial() {
        
        
            if i <= 3{
                if i == 1 {
                    UIView.animate(withDuration: 0.3){
                        self.blueStop.constant = 0
                        self.view.layoutIfNeeded()
                    }
                }
                else if i == 2 {
                    UIView.animate(withDuration: 0.3){
                        self.orangeStops.constant = 0
                        self.legendStops.constant = 0
                        self.view.layoutIfNeeded()
                    }
                }
                else if i == 3 {
                    buttonClose.setTitle("Zakończ", for: .normal)
                    UIView.animate(withDuration: 0.3){
                        self.nearestStop.constant = 0
                        self.view.layoutIfNeeded()
                    }
                }

            }
            else {
                self.dismiss(animated: true, completion: nil)
            }
            i += 1
        

    }
    
    
    
    @IBAction func endTutorial(_ sender: Any) {
        //self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
        //self.dismiss(animated: true, completion: nil)
        let lightImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: .light)
        
        // Prepare shortly before playing
        lightImpactFeedbackGenerator.prepare()
        
        // Play the haptic signal
        lightImpactFeedbackGenerator.impactOccurred()
        showingTutorial()
    }
    

}
