//
//  ThirdViewController.swift
//  Tram Stop
//
//  Created by Michał Hęćka on 17/09/2018.
//  Copyright © 2018 Michał Hęćka. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    var error = ""
    
    @IBOutlet weak var imageDisplayed: UIImageView!
    @IBOutlet weak var Error: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let stopImage = "appError\(Int(arc4random_uniform(UInt32(3))))"
        imageDisplayed.image = UIImage(named: stopImage)
        
        print(stopImage)
        Error.text = error
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func swipeErrors(_ sender: Any) {
        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)

    }
    
    
    @IBAction func CloseErrors(_ sender: Any) {
        
        let lightImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: .light)
        
        // Prepare shortly before playing
        lightImpactFeedbackGenerator.prepare()
        
        // Play the haptic signal
        lightImpactFeedbackGenerator.impactOccurred()
        
        
        self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
    
    }
    

}
