//
//  arrivalCell.swift
//  Tram Stop
//
//  Created by Michał Hęćka on 05/03/2019.
//  Copyright © 2019 Michał Hęćka. All rights reserved.
//

import UIKit

class arrivalCellController: UITableViewCell {

    
    @IBOutlet weak var gpsNotif: UIImageView!
    
    @IBOutlet weak var lowEntrance: UIImageView!
    
    @IBOutlet weak var ticketMachine: UIImageView!
    
    @IBOutlet weak var lineDirection: UILabel!
    
    @IBOutlet weak var line: UILabel!
    
    @IBOutlet weak var time: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code goes here
        
        
        
    }

}
