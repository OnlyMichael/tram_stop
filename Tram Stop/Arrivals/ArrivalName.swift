//
//  ArrivalName.swift
//  Tram Stop
//
//  Created by Michał Hęćka on 01/04/2019.
//  Copyright © 2019 Michał Hęćka. All rights reserved.
//

import UIKit

class ArrivalName: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBOutlet weak var name: UILabel!
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
