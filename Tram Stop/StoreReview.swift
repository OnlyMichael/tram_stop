//
//  StoreReview.swift
//  Tram Stop
//
//  Created by Michał Hęćka on 16/06/2019.
//  Copyright © 2019 Michał Hęćka. All rights reserved.
//


import StoreKit


class ReviewMe {
    
    //UserDefaults dictionary key where we store the number of launching the app
    let launchCountDefaultsKey = "Launches"
    
    //Get UserDefaults value if Its nil or no value found, set the value and there on increment "launchCount" on every launch
    func isReviewViewToBeDisplayed(minimumLaunchCount : Int) -> Bool {
        let launchCount = UserDefaults.standard.integer(forKey: launchCountDefaultsKey)
        
//        if launchCount >= minimumLaunchCount {
//            print("App was opened \(launchCount) times")
//            return true
//        } else {
//            //increase launch count by 1 after every launch
//            UserDefaults.standard.set((launchCount + 1), forKey: launchCountDefaultsKey)
//        }
        if launchCount == minimumLaunchCount {
            UserDefaults.standard.set((launchCount + 1), forKey: launchCountDefaultsKey)
            print("App was opened \(launchCount) times")
            return true
        }
        UserDefaults.standard.set((launchCount + 1), forKey: launchCountDefaultsKey)
        
        print("App was opened \(launchCount) times")
        
        return false
    }
    
    //This method is called from class with minimum launch count needed
    func showReviewView(afterMinimumLaunchCount : Int){
        if (self.isReviewViewToBeDisplayed(minimumLaunchCount : afterMinimumLaunchCount)){
            SKStoreReviewController.requestReview()
        }
    }
    //call "showReviewView" method with desired launch counts needed
/* if #available(iOS 10.3, *){
    ReviewMe.showReviewView(afterMinimumLaunchCount : 2)
 }else{
    //Review View is not available on this version of iOS
 }
 */
}

