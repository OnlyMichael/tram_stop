//
//  RoutesDirectionTableViewCell.swift
//  Tram Stop
//
//  Created by Michał Hęćka on 31/03/2019.
//  Copyright © 2019 Michał Hęćka. All rights reserved.
//

import UIKit

class RoutesDirectionCellController: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    @IBOutlet weak var direction: UILabel!
    @IBOutlet weak var line: UILabel!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
