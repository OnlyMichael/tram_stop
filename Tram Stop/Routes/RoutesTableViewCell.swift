//
//  RoutesTableViewCell.swift
//  Tram Stop
//
//  Created by Michał Hęćka on 30/03/2019.
//  Copyright © 2019 Michał Hęćka. All rights reserved.
//

import UIKit

class RoutesTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var info: UILabel!
    @IBOutlet weak var stop: UILabel!
    @IBOutlet weak var backGround: UIView!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
